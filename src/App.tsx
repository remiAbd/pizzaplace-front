import {Routes, Route, Navigate} from "react-router-dom"
import '../public/stylesheets/css/final.css'
//import "../public/stylesheets/App.css"

import RequireAuth from "./helpers/require-data-auth.tsx"
import Header from "./components/header.tsx"
import Footer from "./components/footer.tsx"

import Home from "./containers/home.tsx"
import Login from "./containers/user/login.tsx"
import Register from "./containers/user/register.tsx"
import Forgot from "./containers/user/forgot.tsx"
import Profil from "./containers/user/profil.tsx"
import Logout from "./containers/user/logout.tsx"
import Products from "./containers/products.tsx"
import Basket from "./containers/panier.tsx"
import Admin from "./containers/admin/admin.tsx"
import Payment from "./containers/payment.tsx"

import CurrentOrders from "./containers/admin/orders/current-orders.tsx"
import Order from "./containers/admin/orders/order-order.tsx"
import PastOrders from "./containers/admin/orders/past-orders.tsx"
import SuccesPayment from "./containers/succes-payment.tsx"


function App() {

    return (
    	<>
			<Header/>
			<Routes>
				<Route path="/" element={<RequireAuth child={Home} auth={false} employee={false} admin={false}/>} />
				<Route path="/login" element={<RequireAuth child={Login} auth={false} employee={false} admin={false}/>} />
				<Route path="/register" element={<RequireAuth child={Register} auth={false} employee={false} admin={false}/>} />
				<Route path="/forgot" element={<RequireAuth child={Forgot} auth={false} employee={false} admin={false}/>} />
				<Route path="/profil" element={<RequireAuth child={Profil} auth={true} employee={false} admin={false}/>} />
				<Route path="/logout" element={<RequireAuth child={Logout} auth={true} employee={false} admin={false}/>} />
				<Route path="/products" element={<RequireAuth child={Products} auth={false} employee={false} admin={false}/>} />
				<Route path="/basket" element={<RequireAuth child={Basket} auth={false} employee={false} admin={false}/>} />
				<Route path="/admin" element={<RequireAuth child={Admin} auth={false} employee={false} admin={true}/>} />
				<Route path="/basket/payment" element={<RequireAuth child={Payment} auth={false} employee={false} admin={false}/>} />
				<Route path="/order/current" element={<RequireAuth child={CurrentOrders} auth={false} employee={true} admin={false}/>} />
				<Route path="/order/order" element={<RequireAuth child={Order} auth={false} employee={true} admin={false}/>} />
				<Route path="/order/past" element={<RequireAuth child={PastOrders} auth={false} employee={true} admin={false}/>} />
				<Route path="/payment/success" element={<RequireAuth child={SuccesPayment} auth={false} employee={false} admin={false}/>} />
				

				
				<Route path="*" element={<Navigate to="/" />} />
			</Routes>
			<Footer/>
    	</>
  	)
}

export default App
