import {Navigate, useParams} from "react-router-dom"
import { useDispatch, useSelector } from "react-redux";
import { loginUser, selectUser } from "../slices/user-slices";
import {useState, useEffect} from "react"

import { checkToken } from "../api/token";
import { changeProducts, selectProducts } from "../slices/products-slices";
import { changeCategories, selectCategories } from "../slices/categories-slices";
import { getAllCategories, getAllProducts } from "../api/products-api";
// import Popup from "../components/popup/popup";

export default function RequireAuth (props : any) {
    const dispatch : any = useDispatch()
    const user = useSelector(selectUser)
    const Child = props.child
    const params : any = useParams();
    const products : any = useSelector(selectProducts)
    const categories : any = useSelector(selectCategories)
    const [redirect, setRedirect] = useState <boolean> (false)
    // const [showPopup, setShowPopup] = useState <Boolean> (false)

    useEffect(() => {
        setRedirect(false)
        if (products.products.length === 0) {
            getAllProducts()
            .then((res : any) => {
                if (res.status === 200) {
                    dispatch(changeProducts(res.results));
                }
            })
            .catch((err : any) => console.log(err));
        }
        if (categories.categories.length === 0) {
            getAllCategories()
            .then((res: any) => {
                if (res.status === 200) {
                    dispatch(changeCategories(res.results));
                }
            })
            .catch((err : any) => {
                console.log(err)
            })
        }


        //On essaye de récupérer le token de connexion
        const token : string | null = window.localStorage.getItem("userToken")
        //Si le token est null et que la page impose d'être connecté, on redirige
        if (token === null && (props.auth || props.admin || props.employee)) {
            setRedirect(true)
        } else {
            //Si l'utilisateur est deconnecté
            if(!user.isLogged) {
                //On vérifie le token
                checkToken()
                .then((res : any) => {
                    //Si le status de la réponse n'est pas 200 et que la route est protégée, on redirige
                    if(res.status !== 200) {
                        if(props.auth || props.employee || props.admin) {
                            setRedirect(true)
                        }
                    } else {
                        //si le status est 200, alors on connecte l'utilisateur
                        if(res.user.role === "customer" && (props.employee || props.admin)) {
                            setRedirect(true)
                        } else if (res.user.role === "employee" && props.admin) {
                            setRedirect(true)
                        } else {
                            let myUser : any = res.user
                            myUser.token = token
                            dispatch(loginUser(myUser))
                        }
                    }
                })
                .catch((err : any) => {
                    console.log(err)
                })
            }
        }
        
    }, [props])

    if(redirect) {
        return <Navigate to="/"/>
    }

    return (
        <>
            <Child {...props} params={params} />   
            {/* <Popup isOpen={showPopup} onClose={closePopup} content={
                <>
                    <h2>Voulez vous accepter les cookies ?</h2>
                    <p>Les cookies sont importants pour la navigation du site, nous ne sauvegardons aucunes données sensibles et ne proposons pas de publicitées</p>
                    <button
                        onClick={refused}
                    >Refuser</button>
                    <button
                        onClick={accepted}
                    >Accepter</button>
                </>
            }/> */}
        </>
    )
}