import { createSlice } from "@reduxjs/toolkit";

let initialBasket : any = JSON.parse(window.localStorage.getItem("basket"))
if(initialBasket === null){
    initialBasket = [] 
}

function calculTotal(basket : any){
    let prix = 0
    for(let i=0; i < basket.length; i++){
        prix += parseInt(basket[i].quantity)* parseFloat(basket[i].product.price)
    }
    return prix
}

const prixTotal = calculTotal(initialBasket)


const initialState = {
    basket : initialBasket,
    totalPrice : prixTotal
}

export const basketSlice = createSlice({
    name : "basket",
    initialState,
    reducers : {
        changeBasket: (state, action) => {
            const prixTotal = calculTotal(action.payload)
            state.basket = action.payload
            state.totalPrice = prixTotal
        },
        deleteBasket: (state) => {
            state.basket = []
            state.totalPrice = 0
        }
    }

})

export const {changeBasket, deleteBasket} = basketSlice.actions 

export const selectBasket = (state: { basket: any; }) => state.basket

export default basketSlice.reducer
