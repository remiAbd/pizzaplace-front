import {configureStore} from '@reduxjs/toolkit'

//import les slices
import userReducer from './user-slices'
import productReducer from './products-slices'
import BasketReducer from './basket-slices'
import categorieReducer from './categories-slices'
import orderReducer from "./orders-slices"



//retourne les reducers
const store = configureStore ({
    reducer : {
        basket: BasketReducer,
        products: productReducer,
        categories: categorieReducer,
        user: userReducer,
        orders : orderReducer
    }
})

export default store
