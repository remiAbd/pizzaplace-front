import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    orders : []
}

export const ordersSlices = createSlice({
    name : "orders",
    initialState,
    reducers : {
        changeOrders : (state, action) => {
            state.orders = action.payload
        },
        deleteOrders : (state) => {
            state.orders = []
        }
    }
})

export const {changeOrders, deleteOrders} = ordersSlices.actions
export const selectOrders = (state: { orders: any }) => state.orders
export default ordersSlices.reducer