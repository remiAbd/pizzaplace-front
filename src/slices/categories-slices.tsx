import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    categories : []
}

export const categoriesSlice = createSlice({
    name : "categories",
    initialState,
    reducers : {
        changeCategories: (state, action) => {
            state.categories = action.payload
        },
        deleteCategories: (state) => {
            state.categories = []
        }
    }

})

export const {changeCategories, deleteCategories} = categoriesSlice.actions 

export const selectCategories = (state: { categories: any; }) => state.categories

export default categoriesSlice.reducer
