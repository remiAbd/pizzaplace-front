import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    products : []
}

export const productsSlices = createSlice({
    name : "products",
    initialState,
    reducers : {
        changeProducts : (state, action) => {
            state.products = action.payload
        },
        deleteProducts : (state) => {
            state.products = []
        }
    }
})

export const {changeProducts, deleteProducts} = productsSlices.actions
export const selectProducts = (state: { products: any }) => state.products
export default productsSlices.reducer

