import {Elements} from "@stripe/react-stripe-js"
import {loadStripe} from "@stripe/stripe-js"
import { useState, useEffect } from "react"
import CheckoutForm from "../components/checkout-form"
import { useLocation } from "react-router-dom"
import { getFullDetails, getOneOrder } from "../api/orders-api"
import { useSelector } from "react-redux"
import { selectUser } from "../slices/user-slices"

export default function Payment(props : any) {
    //useLocation permet de récupérer des variables lors de la redirection
    const location = useLocation()
    const orderId = location.state.orderId
    //la clé publique de stripe me permet de brancher l'environnement de l'api stripe à mon compte stripe API
    const stripePromise = loadStripe("pk_test_51NSJKxDAi9rs7GgE0H15jAodO4IJvtSwmylTYmknRGGKzKqy4w2eHJdSsVQLYuqrzNsepBrScs3ByIWgNSttaPTb00a4jDTHNe")
    const [order, setOrder] = useState <any> ({})
    const [orderDetails, setOrderDetails] = useState <any> ([])
    const [message, setMessage] = useState <string> ("")
    //On récupère la variable redux user
    const user = useSelector(selectUser)

    //Au chargement du dom et de la state orderId
    useEffect(() => {
        //On définit le title de la page
        document.title= "Pizzaplace | Paiement"
        //On set le message d'erreur à vide
        setMessage("")
        //On récupère du backend la commande à payer
        getOneOrder(orderId)
        .then((res) => {
            //Si on récupère bien la commande
            if (res.status === 200) {
                //On stock la commande dans la state order
                setOrder(res.results)
                //On récupère du backend les détails de la commande
                getFullDetails(orderId)
                .then((response) => {
                    //Si on les récupère bien
                    if (res.status === 200) {
                        //On stock dans la state orderDetails le détail de la commande
                        setOrderDetails(response.results)
                    //Sinon
                    } else {
                        //On met à jours le message d'erreur avec la réponse du backend
                        setMessage(response.msg)
                    }
                })
                .catch(err => console.log(err))
            //Sinon
            } else {
                //on met à jours le message d'erreur avec la réponse du backend
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }, [orderId]) 
    
    return (
    <main className="payment">
        <div className="title-box">
            <h1>Paiement</h1>
        </div>
        {/* <p>Id de la commande: {orderId}</p> */}
        {message && <p>{message}</p>}
        <section className="recap">
            <h2>Récapitulatif de la commande :</h2>
            {order.addressLineOne ? <p>Commande en livraison </p>: <p>Commande à emporter</p>}
            <div>
                {orderDetails.map((detail : any) => {
                    return(
                        <p key={detail.ID}>{detail.quantity} x {detail.name}</p>
                    )
                })}
            </div>
            <h3>Prix total : {order.amount}€</h3>
        </section>
        {order.addressLineOne && 
        <section className="address">
            <h2>Adresse de livraison</h2>
            <p>{order.addressLineOne}, {order.postalCode} {order.city}</p>
            <p>{order.addressLineTwo}</p>
            <p>{order.phone}</p>
        </section>
        }
        

        {/*On va brancher l'environnement des fonctionnalitées de react-stripe
            qui va permettre d'effectuer les échanges avec l'api stripe de manière sécurisée
        */}
        <Elements stripe={stripePromise}>
            <CheckoutForm orderId={orderId} />
        </Elements>
    </main>)
}