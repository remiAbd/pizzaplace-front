import { getRestaurant } from "../api/restaurant-api"
import {useState, useEffect} from "react"
// import GoogleMapReact from "google-map-react"
import { config } from "../config"
import MapContainer from "../components/map"
// import { AdvancedMarkerView } from "google-maps-react"

export default function Home() {
    const [restaurant, setRestaurant] = useState <any> ({})
    const [error , setError] = useState <string> ("")
    const [userLatitude, setUserLatitude] = useState <number> (0)
    const [userLongitude, setUserLongitude] = useState <number> (0)
    // const [isGoogleMapsApiLoaded, setIsGoogleMapsApiLoaded] = useState <boolean> (false);

    //Au chargement du dom,
    useEffect(() => {
        //On définit le title de la page
        document.title= "Pizzaplace | Home"
        //On récupère la localisation de l'utilisateur
        getGeoloc()
        //On set par défault l'erreur à une chaine de charactères vides
        setError("")
        //On récupère du backend les informations du restaurant
        getRestaurant()
        .then((res) => {
            //Si on récupère bien le restaurant
            if(res.status === 200) {
                //On stock les informations du restaurant dans la state restaurant
                setRestaurant(res.results)
            //Si on ne récupère pas le restaurant
            } else {
                //On met à jours l'erreur
                setError(res.msg)
            }
        })
        .catch((err) => console.log(err))
        // setIsGoogleMapsApiLoaded(true)
        // console.log("end")
    }, [])

    //Fonction asynchrone qui récupère les coordonnés de l'utilisateur
    async function getGeoloc() {
        await navigator.geolocation.getCurrentPosition((position)=> {
            setUserLatitude(position.coords.latitude)
            setUserLongitude(position.coords.longitude)
        }, (err) => {
            console.log("err : ", err)
        })
    }

    return (
        <main className="home">
            <div className="banner">
                <img className="image-banner" alt="pizza" src={config.pict_url + "pizza-banner.jpg"} />
            </div>
            <div className="title-box">
                <h1>{restaurant.name}</h1>
                {/* <p>{restaurant.content}</p> */}
            </div>
            {error && <p>{error}</p>}

            {/* Map centrée sur l'ordinateur de l'utilisateur s'il a accepté la localisation, sinon la mapo sera centrée sur le restaurant */}
            
            <section className="map">
                <h2>Adresse</h2>
                <section className="content">
                {restaurant &&
                <>
                    <p>{restaurant.addressLineOne}, {restaurant.postalCode} {restaurant.city}</p>
                    <div className="map-box">
                            {userLatitude && userLongitude && restaurant ? 
                                <MapContainer markerPosition={{lat : restaurant.latitude, lng : restaurant.longitude}} initial={{lat : userLatitude, lng : userLongitude}}/>
                            : 
                                <MapContainer markerPosition={{lat : restaurant.latitude, lng : restaurant.longitude}} initial={{lat : restaurant.latitude, lng : restaurant.longitude}}/>
                            }
                    </div>
                </>
                }
                </section>
            </section>

            {/* Calendrier des horaires d'ouvertures du restaurant*/}
            <section className="horaires">
                
                <section className="content">
                    <h2>Horaires</h2>
                    {restaurant.startLunchMonday && restaurant.startDinnerMonday ? 
                        <p>Lundi : de {restaurant.startLunchMonday} à {restaurant.endLunchMonday} et {restaurant.startDinnerMonday} à {restaurant.endDinnerMonday}</p>
                    : 
                        <p>Lundi : fermé</p>
                    }
                    {restaurant.startLunchTuesday && restaurant.startDinnerTuesday ? 
                        <p>Mardi : de {restaurant.startLunchTuesday} à {restaurant.endLunchTuesday} et {restaurant.startDinnerTuesday} à {restaurant.endDinnerTuesday}</p>
                    : 
                        <p>Mardi : fermé</p>
                    }
                    {restaurant.startLunchWednesday && restaurant.startDinnerWednesday ? 
                        <p>Mercredi : de {restaurant.startLunchWednesday} à {restaurant.endLunchWednesday} et {restaurant.startDinnerWednesday} à {restaurant.endDinnerWednesday}</p>
                    : 
                        <p> Mercredi : fermé</p>
                    }
                    {restaurant.startLunchThursday && restaurant.startDinnerThursday ? 
                        <p>Jeudi : de {restaurant.startLunchThursday} à {restaurant.endLunchThursday} et {restaurant.startDinnerThursday} à {restaurant.endDinnerThursday}</p>
                    : 
                        <p>Jeudi : fermé</p>
                    }
                    {restaurant.startLunchFriday && restaurant.startDinnerFriday ? 
                        <p>Vendredi : de {restaurant.startLunchFriday} à {restaurant.endLunchFriday} et {restaurant.startDinnerFriday} à {restaurant.endDinnerFriday}</p>
                    : 
                        <p>Vendredi : fermé</p>
                    }
                    {restaurant.startLunchSaturday && restaurant.startDinnerSaturday ? 
                        <p>Samedi : de {restaurant.startLunchSaturday} à {restaurant.endLunchSaturday} et {restaurant.startDinnerSaturday} à {restaurant.endDinnerSaturday}</p>
                    : 
                        <p>Samedi : fermé</p>
                    }
                    {restaurant.startLunchSunday && restaurant.startDinnerSunday ? 
                        <p>Dimanche : de {restaurant.startLunchSunday} à {restaurant.endLunchSunday} et {restaurant.startDinnerSunday} à {restaurant.endDinnerSunday}</p>
                    : 
                        <p>Dimanche : fermé</p>
                    }
                </section>
            </section>
        </main>
    )
}