import { useEffect, useState } from "react"
import { useLocation } from "react-router-dom"
import { getFullDetails, getOneOrder } from "../api/orders-api"
import { getRestaurant } from "../api/restaurant-api"

export default function SuccesPayment(props: any) {
    const location = useLocation()
    const orderId = location.state.orderId

    const [message, setMessage] = useState <string> ("")
    const [order, setOrder] = useState <any> (null)
    const [details, setDetails] = useState <any> (null)
    const [restaurant, setRestaurant] = useState <any> (null)

    useEffect(() => {
        getOneOrder(orderId)
        .then((resOrder) => {
            if(resOrder.status === 200) {
                setOrder(resOrder.results)
                getFullDetails(orderId)
                .then((resDetails) => {
                    if(resDetails.status === 200) {
                        setDetails(resDetails.results)
                        getRestaurant()
                        .then((resRestaurant) => {
                            setRestaurant(resRestaurant)
                        })
                        .catch(err => console.log(err))
                    } else {
                        setMessage(resDetails.msg)
                    }
                })
                .catch(err => console.log(err))
            } else {
                setMessage(resOrder.msg)
            }
        })
        .catch(err => console.log(err))
    }, [orderId])

    
    return (
        <main className="success">
            <div className="title-box">
                <h1>Merci pour votre commande !</h1>
            </div>
            <section className="order-box">
                <h2>Récapitulatif de votre commande :</h2>
                {details && 
                    <>
                        {details.map((detail : any, index : number)=> {
                            if(index%2===0) {
                                return(
                                    <p 
                                        key={index} 
                                        className="white"
                                    >{detail.name}</p>
                                )
                            } else {
                                return(
                                    <p 
                                        key={index} 
                                        className="grey"
                                    >{detail.name}</p>
                                )
                            }
                        })}
                    </>
                }
            </section>
            {order &&
                <>
                {order.addressLineOne ? 
                <section>
                    <h2>Livraison</h2>
                    <p>Cette commande est livrée chez vous</p>
                </section>
                : 
                <section>
                    <h2>À remporter</h2>
                    <p>Cette commande doit être récupérée au restaurant</p>
                    {restaurant && 
                        <h3>{restaurant.addressLineOne}, {restaurant.postalCode} {restaurant.city}</h3>
                    }
                    <p></p>
                </section>
                }
                <section>
                    <h2>Status de la commande :</h2>
                    <p>{order.status}</p>
                </section>

                </>
            }   
        </main>
    )
}