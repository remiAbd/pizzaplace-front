import { useDispatch, useSelector } from "react-redux"
import { useEffect, useState } from "react"
import { changeBasket, selectBasket } from "../slices/basket-slices"
import ShowProduct from "./admin/products/show-product"


import Popup from "../components/popup/popup"

import { config } from "../config"

export default function ProductDetails(props : any) {
    //On importe useDispatch qui servira a modifier les variables redux
    const dispatch = useDispatch()
    //On importe la variable panier de redux
    const basket : any = useSelector(selectBasket)
    
    const [quantity, setQuantity] = useState <number> (0)
    const [showPopup, setShowPopup] = useState <Boolean> (false)
    const [content, setContent] = useState <string> ("")
    const [showProductDetails, setShowProductDetails] = useState <boolean> (false)
    const [productIdToShow, setProductIdToShow] = useState <number> (0)

    //S'il y a une quantité
    if(props.quantity) {
        useEffect(() => {
            setQuantity(props.quantity)
        }, [])
    }

    //Fonction de changement du panier
    function changeBakset(product : any, basket : any, ajout : Boolean) {
        //On récupère le panier en permettant la modification
        let myBasket : any = JSON.parse(JSON.stringify(basket))
        //Si le produit est déja dans le panier, alors on récupère son index dans le tableau, sinon l'index sera -1
        let index : number = myBasket.findIndex((existingProduct :any) => existingProduct.product.id === product.id)
        //Si on veux ajouter dans le panier
        if (ajout) {
            //Si le produit n'est pas déja dans le panier
            if (index === -1) {
                //On indente le tableau avec le nouveau produit
                myBasket.push({
                    product : product,
                    quantity : 1
                })
                //On modifie la state quantity
                setQuantity(myBasket[myBasket.length-1].quantity)
            //Si le produit est déja dans le tableau
            } else {
                //On rajoute une quantité dans le tableau à l'index trouvé plus haut
                myBasket[index].quantity += 1
                //On modifie la state quantity
                setQuantity(myBasket[index].quantity)
            }

            setContent("Produit ajouté au panier.")
        } else {
            if (myBasket[index].quantity > 1) {
                myBasket[index].quantity -= 1
                setQuantity(myBasket[index].quantity)
            } else {
                myBasket.splice(index, 1)
            }
            //On modifie le message de la popup
            setContent("Produit supprimé du panier.")
        }
        //On stock dans le localStorage le nouveau panier après modification
        window.localStorage.setItem("basket", JSON.stringify(myBasket));
        //On change la variable redux du panier
        dispatch(changeBasket(myBasket))
        //On ouvre le popup
        openPopup()
        //au bout de 2000ms, on ferme le popup
        setTimeout(() => {
            closePopup()
        }, 2000)
    }

    //Fonction de fermeture du popup
    function closePopup() {
        setShowPopup(false)
    }

    //Fonction d'ouverture du popup
    function openPopup() {
        setShowPopup(true)
    }

    //Fonction de fermeture de la popup de détail du produit
    function closeDetailPopup() {
        setShowProductDetails(false)
    }

    return (
        <>
            {!props.isBasket && 
            <div className="product">
                <div className="product-picture"><img className="image" alt={props.product.alt} src={config.pict_url + props.product.url}/></div>
                <div className="margin-left">
                    <button
                        className="invisible-button"
                        onClick={(e) => {
                            e.preventDefault()
                            setProductIdToShow(props.product.id)
                            setShowProductDetails(!showProductDetails)
                        }}
                    ><h3>{props.product.name}</h3></button>
                    <p>{props.product.ingredients}</p>
                    <div className="price-button">
                        <p className="price">{props.product.price} €</p>
                        <button
                            onClick={() => {
                                changeBakset(props.product, basket.basket, true)
                            }}
                        ><p>J'ai faim !</p></button>
                    </div>
                </div>
            </div>
            }

            {props.isBasket && 
            <div className={`product-basket ${props.backgroundColor}`}>
                <button
                    className="invisible-button"
                    onClick={(e) => {
                        e.preventDefault()
                        setProductIdToShow(props.product.id)
                        setShowProductDetails(!showProductDetails)
                    }}
                >
                    <img className="background-img" alt={props.product.alt} src={config.pict_url + props.product.url}/>
                    <h3>{props.product.name}</h3>
                </button>
                <div className="vertical">
                    <p><em>Quantitée :</em></p>
                    <div className="quantity-basket">
                        <button
                            className="button-white"
                            onClick={() => {
                                changeBakset(props.product, basket.basket, false)
                            }}
                        >-1</button>
                        {/* <input type="number" value={quantity} onChange={(e) => {
                            e.preventDefault()
                            setQuantity(parseInt(e.currentTarget.value))
                        }}/> */}
                        <p className="red">{quantity}</p>
                        <button
                            className="button-white"
                            onClick={() => {
                                changeBakset(props.product, basket.basket, true)
                            }}
                        >+1</button>
                    </div>
                </div>
                <div className="price">
                    <p><em>Prix</em></p>
                    <p>{props.product.price * quantity} €</p>
                </div>
            </div>
                
            }
            {showPopup &&
            <div className="timed-small-popup">
                    <Popup
                        isOpen={showPopup}
                        onClose={closePopup}
                        content={
                            content
                        }
                    />
            </div>
            }
            {showProductDetails &&
                <ShowProduct 
                onClose={closeDetailPopup}
                productId = {productIdToShow}
            />
            }
        </>
    )
}