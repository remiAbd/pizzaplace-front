import { useState } from "react"

import { registerOneCustomer } from "../../../api/user-api"

export default function AddCustomer(props : any) {
    const [firstName, setFirstName] = useState <string> ("")
    const [lastName, setLastName] = useState <string> ("")
    const [email, setEmail] = useState <string> ("")
    const [phone, setPhone] = useState <string> ("")
    const [password1, setPassword1] = useState <string> ("")
    const [password2, setPassword2] = useState <string> ("")
    const [addressLineOne, setAddressLineOne] = useState <string> ("")
    const [addressLineTwo, setAddressLineTwo] = useState <string> ("")
    const [postalCode, setPostalCode] = useState <string> ("")
    const [city, setCity] = useState <string> ("")
    const [message, setMessage] = useState <string> ("")

    //Fonction d'envoi du formulaire
    function handleSubmit() {
        //Si tous les champs obligatoires sont remplis
        if(firstName && lastName && email && phone && password1 && password2 && addressLineOne && postalCode && city) {
            //Si les mots de passes concordent
            if (password1 === password2) {
                //On construit l'objet d'envoi
                let datas : {
                    firstName : string,
                    lastName : string,
                    email : string,
                    phone : string,
                    password : string,
                    addressLineOne : string,
                    addressLineTwo : string,
                    postalCode : string,
                    city : string
                } = {
                    firstName : firstName,
                    lastName : lastName,
                    email : email, 
                    phone : phone,
                    password : password1,
                    addressLineOne : addressLineOne,
                    addressLineTwo : addressLineTwo,
                    postalCode : postalCode,
                    city : city
                }
                //On enregistre le client
                registerOneCustomer(datas)
                .then((res) => {
                    //S'il est bien enregistré
                    if(res.status === 200) {
                        //On actualise le message d'erreur dans le code parent
                        props.updateMessage(res.msg)
                        //On ferme le popup
                        props.onClose()
                    //Sinon
                    } else {
                        //On actualise le message d'erreur
                        setMessage(res.msg)
                    }
                })
                .catch((err) => {
                    console.log(err)
                })
            //sinon
            } else {
                //On actualise le message d'erreur
                setMessage("Les mots de passe ne correspondent pas")
            }
        //sinon,
        } else {
            //On actualise le message d'erreur
            setMessage("Veuillez remplir tous les champs munis d'un * svp")
        }
    }


    return (
        <form className="classic-form big-popup">
            <h1>Ajouter un client</h1>
            {message && <p>{message}</p>}

            <label htmlFor="firstName">Prénom</label>
            <input 
                type="text"
                name="firstName"
                placeholder="Prénom"
                onChange={(e) => {
                    setFirstName(e.currentTarget.value);
                }}
            />

            <label htmlFor="lastName">Nom</label>
            <input 
                type="text"
                name="lastName"
                placeholder="Nom"
                onChange={(e) => {
                    setLastName(e.currentTarget.value);
                }}
            />

            <label htmlFor="email">Adresse e-mail</label>
            <input 
                type="text"
                name="email"
                placeholder="Adresse e-mail"
                onChange={(e) => {
                    setEmail(e.currentTarget.value);
                }}
            />

            <label htmlFor="phone">Numéro de téléphone</label>
            <input 
                type="text"
                name="phone"
                placeholder="Numéro de téléphone"
                onChange={(e) => {
                    setPhone(e.currentTarget.value);
                }}
            />

            <label htmlFor="password1">Mot de passe</label>
            <input 
                type="password"
                name="password1"
                placeholder="Mot de passe"
                onChange={(e) => {
                    setPassword1(e.currentTarget.value);
                }}
            />

            <label htmlFor="password2">Confirmez le mot de passe</label>
            <input 
                type="password"
                name="password2"
                placeholder="Confirmez le mot de passe"
                onChange={(e) => {
                    setPassword2(e.currentTarget.value);
                }}
            />

            <label htmlFor="address1">Adresse</label>
            <input 
                type="text"
                name="address1"
                placeholder="Adresse"
                onChange={(e) => {
                    setAddressLineOne(e.currentTarget.value);
                }}
            />

            <label htmlFor="address2">Compléments d'adresse</label>
            <input 
                type="text"
                name="address2"
                placeholder="Compléments d'adresse"
                onChange={(e) => {
                    setAddressLineTwo(e.currentTarget.value);
                }}
            />

            <label htmlFor="postalCode">Code postal</label>
            <input 
                type="text"
                name="postalCode"
                placeholder="Code postal"
                onChange={(e) => {
                    setPostalCode(e.currentTarget.value);
                }}
            />

            <label htmlFor="city">Ville</label>
            <input 
                type="text"
                name="city"
                placeholder="Ville"
                onChange={(e) => {
                    setCity(e.currentTarget.value);
                }}
            />


            <div className="button-box">
                <button
                    className="two button-white"
                    onClick={(e) => {
                        e.preventDefault()
                        handleSubmit()
                    }}
                >Valider
                </button>
                <button
                    className="two button-white"
                    onClick={(e) => {
                        e.preventDefault()
                        props.onClose()
                    }}
                >Annuler</button>
            </div>
        </form>
    )
}