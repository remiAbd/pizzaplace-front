import { useState, useEffect } from "react";
import { editOneCustomer, getOneCustomer } from "../../../api/user-api";

export default function EditCustomer(props: any) {
    const [firstName, setFirstName] = useState<string>("");
    const [lastName, setLastName] = useState<string>("");
    const [email, setEmail] = useState<string>("");
    const [phone, setPhone] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [addressLineOne, setAddressLineOne] = useState<string>("");
    const [addressLineTwo, setAddressLineTwo] = useState<string>("");
    const [postalCode, setPostalCode] = useState<string>("");
    const [city, setCity] = useState<string>("");

    const [message, setMessage] = useState<string>("");

    //Au chargement du dom
    useEffect(() => {
        //On récupère le client
        getOneCustomer(props.key_id)
            .then((res) => {
                //s'il est bien récupéré
                if (res.status === 200) {
                    //On met à jours les states avec les informations du client
                    setFirstName(res.results.firstName);
                    setLastName(res.results.lastName);
                    setEmail(res.results.email);
                    setPhone(res.results.phone);
                    setAddressLineOne(res.results.addressLineOne);
                    setAddressLineTwo(res.results.addressLineTwo);
                    setPostalCode(res.results.postalCode);
                    setCity(res.results.city);
                    //Sinon,
                } else {
                    //On met à jours le message d'erreur
                    setMessage(res.msg);
                }
            })
            .catch((err) => console.log(err));
    }, [props]);

    //Fonction de gestion de formulaire
    function handleSubmit() {
        //On construit l'objet d'envoi
        let datas: {
            firstName: string;
            lastName: string;
            email: string;
            phone: string;
            password: string;
            addressLineOne: string;
            addressLineTwo: string;
            postalCode: string;
            city: string;
        } = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            phone: phone,
            password: password,
            addressLineOne: addressLineOne,
            addressLineTwo: addressLineTwo,
            postalCode: postalCode,
            city: city,
        };
        //On modifie l'utilisateur
        editOneCustomer(datas, props.key_id)
            .then((res) => {
                //s'il est bien modifié
                if (res.status === 200) {
                    //On met à jours le message d'erreur dans le code parent
                    props.updateMessage(res.msg);
                    //On ferme le popup
                    props.onClose();
                    //Sinon
                } else {
                    //on met à jours le message d'erreur
                    setMessage(res.msg);
                }
            })
            .catch((err) => console.log(err));
    }

    return (
        <form className="classic-form big-popup">
            <h1>Ajouter un client</h1>
            {message && <p>{message}</p>}

            <label htmlFor="firstName">Prénom:</label>
            <input
                type="text"
                name="firstName"
                placeholder="Prénom"
                defaultValue={firstName}
                onChange={(e) => {
                    setFirstName(e.currentTarget.value);
                }}
            />

            <label htmlFor="lastName">Nom:</label>
            <input
                type="text"
                name="lastName"
                placeholder="Nom"
                defaultValue={lastName}
                onChange={(e) => {
                    setLastName(e.currentTarget.value);
                }}
            />

            <label htmlFor="email">Email:</label>
            <input
                type="text"
                name="email"
                placeholder="Email"
                defaultValue={email}
                onChange={(e) => {
                    setEmail(e.currentTarget.value);
                }}
            />

            <label htmlFor="phone">Téléphone:</label>
            <input
                type="text"
                name="phone"
                placeholder="Téléphone"
                defaultValue={phone}
                onChange={(e) => {
                    setPhone(e.currentTarget.value);
                }}
            />

            <label htmlFor="password">Mot de passe:</label>
            <input
                type="text"
                name="password"
                placeholder="Mot de passe"
                defaultValue={password}
                onChange={(e) => {
                    setPassword(e.currentTarget.value);
                }}
            />

            <label htmlFor="address1">Adresse:</label>
            <input
                type="text"
                name="address1"
                placeholder="Adresse"
                defaultValue={addressLineOne}
                onChange={(e) => {
                    setAddressLineOne(e.currentTarget.value);
                }}
            />

            <label htmlFor="address2">Complément d'adresse:</label>
            <input
                type="text"
                name="address2"
                placeholder="Complément d'adresse"
                defaultValue={addressLineTwo}
                onChange={(e) => {
                    setAddressLineTwo(e.currentTarget.value);
                }}
            />

            <label htmlFor="postalCode">Code postal:</label>
            <input
                type="text"
                name="postalCode"
                placeholder="Code postal"
                defaultValue={postalCode}
                onChange={(e) => {
                    setPostalCode(e.currentTarget.value);
                }}
            />

            <label htmlFor="city">Ville:</label>
            <input
                type="text"
                name="city"
                placeholder="Ville"
                defaultValue={city}
                onChange={(e) => {
                    setCity(e.currentTarget.value);
                }}
            />

            <div className="button-box">
                <button
                    className="button-white two"
                    onClick={(e) => {
                        e.preventDefault();
                        handleSubmit();
                    }}
                >
                    Valider
                </button>
                <button
                    className="button-white two"
                    onClick={(e) => {
                        e.preventDefault();
                        props.onClose();
                    }}
                >
                    Annuler
                </button>
            </div>
        </form>
    );
}
