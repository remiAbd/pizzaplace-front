import { useState, useEffect } from "react"
import { getOneCustomer } from "../../../api/user-api"

export default function ShowCustomer(props : any) {
    const [firstName, setFirstName] = useState <string> ("")
    const [lastName, setLastName] = useState <string> ("")
    const [email, setEmail] = useState <string> ("")
    const [phone, setPhone] = useState <string> ("")
    const [password, setPassword] = useState <string> ("")
    const [addressLineOne, setAddressLineOne] = useState <string> ("")
    const [addressLineTwo, setAddressLineTwo] = useState <string> ("")
    const [postalCode, setPostalCode] = useState <string> ("")
    const [city, setCity] = useState <string> ("")

    const [message, setMessage] = useState <string> ("")

    //Au chargement du dom
    useEffect(() => {
        //On récupère le client
        getOneCustomer(props.key_id)
        .then((res) => {
            //S'il est bien récupéré
            if(res.status === 200) {
                //On met à jours les states avec les informations du client
                setFirstName(res.results.firstName)
                setLastName(res.results.lastName)
                setEmail(res.results.email)
                setPhone(res.results.phone)
                setPassword(res.results.password)
                setAddressLineOne(res.results.addressLineOne)
                setAddressLineTwo(res.results.addressLineTwo)
                setPostalCode(res.results.postalCode)
                setCity(res.results.city)
            //Sinon,
            } else {
                //On ferme le popup et on met à jours le message d'erreur dans le code parent
                props.onClose()
                props.updateMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }, [props])

    return (
        <section className="big-popup">
            <h1>{firstName} {lastName.toUpperCase()}</h1>
            <section className="popup-content">
                <p>Email : {email}</p>
                <p>Telephone : {phone}</p>
                <p>Adresse : {addressLineOne}, {postalCode} {city}</p>
                {addressLineTwo && 
                    <p>Complément d'adresse : {addressLineTwo}</p>
                }   
            </section>

            <button
                className="button-white"  
                onClick={(e) => {
                    e.preventDefault()
                    props.onClose()
                }}
            >Fermer</button>
        </section>
    )
}