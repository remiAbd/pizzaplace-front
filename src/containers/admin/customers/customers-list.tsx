import { useState, useEffect } from "react"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPen, faXmark } from "@fortawesome/free-solid-svg-icons"

import { deleteOneCustomer, getAllCustomers } from "../../../api/user-api"

import ShowCustomer from "./show-customer"
import AddCustomer from "./add-customer"
import EditCustomer from "./edit-customer"

export default function CustomerList(props : any) {
    const [showAddCustomer, setShowAddCustomer] = useState <boolean> (false)
    const [showEditCustomer, setShowEditCustomer] = useState <boolean> (false)
    const [key_id, setKey_id] = useState <number> (0)
    const [message, setMessage] = useState <string> ("")
    const [showCustomerDetails, setShowCustomerDetails] = useState <boolean> (false)
    const [customers, setCustomers] = useState <any> ([])

    //Au chargement du dom et des props
    useEffect(() => {
        //On met à jours customers avec les props
        setCustomers(props.customers)
    }, [props])

    //fonction de changement du message d'erreur
    function updateMessage(message : string) {
        setMessage(message)
    }

    //fonction de fermeture du popup
    function closePopup() {
        setShowAddCustomer(false)
        setShowEditCustomer(false)
        setShowCustomerDetails(false)
        refreshCustomers()
    }

    //Fonction de rafraichissement des clients
    function refreshCustomers() {
        //On récupère les clients
        getAllCustomers()
        .then((res) => {
            //On met à jours la state des clients avec le résultat du backend
            setCustomers(res.results)
        })
        .catch(err => console.log(err))
    }

    //Fonction de suppression d'un client
    function deleteCustomer(key_id : string) {
        //On supprime un client
        deleteOneCustomer(key_id)
        .then((res) => {
            //s'il est bien supprimé
            if (res.status === 200) {
                //On rafraichit la liste des clients
                refreshCustomers()
            //Sinon,
            } else {
                //On actualise le message d'erreur
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }

    return (
        <>
            {message && <p className="status-message">{message}</p>}
            <button
                className="button-white add-button button-right"
                onClick={() => {
                    setShowAddCustomer(!showAddCustomer)
                }}
            >Ajouter un cient</button>

            {customers.length > 0 && 
                <table>
                    <thead>
                        <tr>
                            <td>Nom</td>
                            <td>Télephone</td>
                            <td className="small">Email</td>
                            <td>Boutons</td>
                        </tr>
                    </thead>
                    <tbody>
                        {customers.map((customer : any) => {
                            return (
                                <tr key={customer.customersID}>
                                    
                                    <td>
                                        <button 
                                            className="invisible-button"
                                            onClick={(e) => {
                                                e.preventDefault()
                                                setKey_id(customer.key_id)
                                                setShowCustomerDetails(!showCustomerDetails)
                                                }}
                                        >{customer.firstName + " " + customer.lastName.toUpperCase()}</button>
                                        </td>
                                    <td>{customer.phone}</td>
                                    <td className="small">{customer.email}</td>
                                    
                                    <td>
                                        <button
                                            className="edit-button param-button"
                                            onClick={(e) => {
                                                e.preventDefault()
                                                setKey_id(customer.key_id)
                                                setShowEditCustomer(!showEditCustomer)
                                            }}
                                        ><FontAwesomeIcon icon={faPen}/></button>
                                        <button
                                            className="delete-button param-button"
                                            onClick={(e)=> {
                                                e.preventDefault()
                                                deleteCustomer(customer.key_id)
                                            }}
                                        ><FontAwesomeIcon icon={faXmark}/></button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            }
            {showAddCustomer && 
                <AddCustomer
                    updateMessage={updateMessage}
                    onClose={closePopup}
                />
            }
            {showEditCustomer && 
                <EditCustomer 
                    updateMessage={updateMessage}
                    onClose={closePopup}
                    key_id = {key_id}
                />
            }
            {showCustomerDetails &&
                <ShowCustomer 
                    updateMessage={updateMessage}
                    onClose={closePopup}
                    key_id = {key_id}
            />
            }
        </>
    )
}