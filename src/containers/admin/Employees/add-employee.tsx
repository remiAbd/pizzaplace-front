import { useState } from "react"

import { saveOneEmployee } from "../../../api/user-api"

export default function AddEmployee(props : any) {
    const [firstName, setFirstName] = useState <string> ("")
    const [lastName, setLastName] = useState <string> ("")
    const [email, setEmail] = useState <string> ("")
    const [phone, setPhone] = useState <string> ("")
    const [password1, setPassword1] = useState <string> ("")
    const [password2, setPassword2] = useState <string> ("")
    const [addressLineOne, setAddressLineOne] = useState <string> ("")
    const [addressLineTwo, setAddressLineTwo] = useState <string> ("")
    const [postalCode, setPostalCode] = useState <string> ("")
    const [city, setCity] = useState <string> ("")
    const [message, setMessage] = useState <string> ("")
    const [role, setRole] = useState <string> ("classic")

    //Fonction d'envoi du formulaire
    function handleSubmit() {
        //Si tous les champs obligatoires sont remplis
        console.log(firstName, lastName, phone, email, password1, password2, addressLineOne, postalCode, city, role)
        if(firstName && lastName && email && phone && password1 && password2 && addressLineOne && postalCode && city && role) {
            //Si les mots de passes concordent
            if (password1 === password2) {
                //On construit l'objet d'envoi
                let datas : {
                    firstName : string,
                    lastName : string,
                    email : string,
                    phone : string,
                    password : string,
                    addressLineOne : string,
                    addressLineTwo : string,
                    postalCode : string,
                    city : string,
                    role: string,
                } = {
                    firstName : firstName,
                    lastName : lastName,
                    email : email, 
                    phone : phone,
                    password : password1,
                    addressLineOne : addressLineOne,
                    addressLineTwo : addressLineTwo,
                    postalCode : postalCode,
                    city : city,
                    role: role,
                }
                //On enregistre le client
                saveOneEmployee(datas)
                .then((res) => {
                    //S'il est bien enregistré
                    if(res.status === 200) {
                        //On actualise le message d'erreur dans le code parent
                        props.updateMessage(res.msg)
                        //On ferme le popup
                        props.onClose()
                    //Sinon
                    } else {
                        //On actualise le message d'erreur
                        setMessage(res.msg)
                    }
                })
                .catch((err) => {
                    console.log(err)
                })
            //sinon
            } else {
                //On actualise le message d'erreur
                setMessage("Les mots de passe ne correspondent pas")
            }
        //sinon,
        } else {
            //On actualise le message d'erreur
            setMessage("Veuillez remplir tous les champs munis d'un * svp")
        }
    }


    return (
        <form className="classic-form big-popup">
            <h1>Ajouter un employé</h1>
            {message && <p>{message}</p>}

            <label htmlFor="firstName">Prénom</label>
            <input 
                id="firstName"
                type="text"
                name="firstName"
                placeholder="Prénom"
                onChange={(e) => {
                    setFirstName(e.currentTarget.value);
                }}
            />

            <label htmlFor="lastName">Nom</label>
            <input 
                id="lastName"
                type="text"
                name="lastName"
                placeholder="Nom"
                onChange={(e) => {
                    setLastName(e.currentTarget.value);
                }}
            />

            <label htmlFor="email">Adresse e-mail</label>
            <input 
                id="email"
                type="text"
                name="email"
                placeholder="Adresse e-mail"
                onChange={(e) => {
                    setEmail(e.currentTarget.value);
                }}
            />

            <label htmlFor="phone">Numéro de téléphone</label>
            <input 
                id="phone"
                type="text"
                name="phone"
                placeholder="Numéro de téléphone"
                onChange={(e) => {
                    setPhone(e.currentTarget.value);
                }}
            />

            <label htmlFor="password1">Mot de passe</label>
            <input 
                id="password1"
                type="password"
                name="password1"
                placeholder="Mot de passe"
                onChange={(e) => {
                    setPassword1(e.currentTarget.value);
                }}
            />

            <label htmlFor="password2">Confirmez le mot de passe</label>
            <input 
                id="password2"
                type="password"
                name="password2"
                placeholder="Confirmez le mot de passe"
                onChange={(e) => {
                    setPassword2(e.currentTarget.value);
                }}
            />

            <label htmlFor="address1">Adresse</label>
            <input 
                id="address1"
                type="text"
                name="address1"
                placeholder="Adresse"
                onChange={(e) => {
                    setAddressLineOne(e.currentTarget.value);
                }}
            />

            <label htmlFor="address2">Compléments d'adresse</label>
            <input 
                id="address2"
                type="text"
                name="address2"
                placeholder="Compléments d'adresse"
                onChange={(e) => {
                    setAddressLineTwo(e.currentTarget.value);
                }}
            />

            <label htmlFor="postalCode">Code postal</label>
            <input 
                id="postalCode"
                type="text"
                name="postalCode"
                placeholder="Code postal"
                onChange={(e) => {
                    setPostalCode(e.currentTarget.value);
                }}
            />

            <label htmlFor="city">Ville</label>
            <input 
                id="city"
                type="text"
                name="city"
                placeholder="Ville"
                onChange={(e) => {
                    setCity(e.currentTarget.value);
                }}
            />

            <label htmlFor="role">Role :</label>
            <select
                id="role"
                name="Role"
                onChange={(e) => {
                    e.preventDefault()
                    setRole(e.currentTarget.value)
                }}
            >
                <option defaultValue="classic">Classique</option>
                <option value="delivery">Livreur</option>
                <option value="admin">Administrateur</option>
            </select>

            <div className="button-box">
                <button
                    className="two button-white"
                    onClick={(e) => {
                        e.preventDefault()
                        handleSubmit()
                    }}
                >Valider
                </button>
                <button
                    className="two button-white"
                    onClick={(e) => {
                        e.preventDefault()
                        props.onClose()
                    }}
                >Annuler</button>
            </div>
        </form>
    )
}