import { useState, useEffect } from "react"
import { getAllEmployees, deleteOneEmployee } from "../../../api/user-api"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPen, faXmark } from "@fortawesome/free-solid-svg-icons"

import ShowEmployee from "./show-employee"
import EditEmployee from "./edit-employee"
import AddEmployee from "./add-employee"


export default function EmployeeList(props : any) {
    const [showAddEmployee, setShowAddEmployee] = useState <boolean> (false)
    const [showEditEmployee, setShowEditEmployee] = useState <boolean> (false)
    const [id, setId] = useState <number | null> (null)
    const [message, setMessage] = useState <string> ("")
    const [showEmployeeDetails, setShowEmployeeDetails] = useState <boolean> (false)
    const [employees, setEmployees] = useState <any> ([])

    useEffect(() => {
        console.log("employés ", props.employees)
        setEmployees(props.employees)
    }, [props])

    //fonction de changement du message d'erreur
    function updateMessage(message : string) {
        setMessage(message)
    }

    //fonction de fermeture du popup
    function closePopup() {
        setShowAddEmployee(false)
        setShowEditEmployee(false)
        setShowEmployeeDetails(false)
        refreshEmployees()
    }

    //Fonction de rafraichissement des clients
    function refreshEmployees() {
        //On récupère les clients
        getAllEmployees()
        .then((res) => {
            //On met à jours la state des clients avec le résultat du backend
            if(res.status === 200) {
                setEmployees(res.results)
            } else {
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }

    //Fonction de suppression d'un client
    function deleteEmployee(id : number) {
        //On supprime un client
        deleteOneEmployee(id)
        .then((res) => {
            //s'il est bien supprimé
            if (res.status === 200) {
                //On rafraichit la liste des clients
                refreshEmployees()
            //Sinon,
            } else {
                //On actualise le message d'erreur
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }

    return ( 
    <>
        {message && <p className="status-message">{message}</p>}
        <button
            className="button-white add-button button-right"
            onClick={() => {
                setShowAddEmployee(!showAddEmployee)
            }}
        >Ajouter un Employé</button>

        {employees.length && 
        <table>
            <thead>
                <tr>
                    <td>Nom</td>
                    <td>Télephone</td>
                    <td className="small">Email</td>
                    <td>Boutons</td>
                </tr>
            </thead>
            <tbody>
                {employees.map((employee : any) => {
                    return (
                        <tr key={employee.employeeID}>
                            
                            <td>
                                <button 
                                    className="invisible-button"
                                    onClick={(e) => {
                                        e.preventDefault()
                                        setId(employee.employeeID)
                                        setShowEmployeeDetails(!showEmployeeDetails)
                                        }}
                                >{employee.firstName + " " + employee.lastName.toUpperCase()}</button>
                                </td>
                            <td>{employee.phone}</td>
                            <td className="small">{employee.email}</td>
                            
                            <td>
                                <button
                                    className="edit-button param-button"
                                    onClick={(e) => {
                                        e.preventDefault()
                                        setId(employee.employeeID)
                                        setShowEditEmployee(!showEditEmployee)
                                    }}
                                ><FontAwesomeIcon icon={faPen}/></button>
                                <button
                                    className="delete-button param-button"
                                    onClick={(e)=> {
                                        e.preventDefault()
                                        deleteEmployee(employee.employeeID)
                                    }}
                                ><FontAwesomeIcon icon={faXmark}/></button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        }
        {showAddEmployee && 
            <AddEmployee
                updateMessage={updateMessage}
                onClose={closePopup}
            />
        }
        {showEditEmployee && 
            <EditEmployee 
                updateMessage={updateMessage}
                onClose={closePopup}
                id = {id}
            />
        }
        {showEmployeeDetails &&
            <ShowEmployee 
                updateMessage={updateMessage}
                onClose={closePopup}
                id = {id}
        />
        }
    </>
    )
}