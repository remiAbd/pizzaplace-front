import { useState, useEffect } from "react"
import { getOneEmployee } from "../../../api/user-api"

export default function ShowEmployee(props : any) {
    //State employee servant à stocker les informations de l'employé qu'on montre
    const [employee, setEmployee] = useState <any> (null)

    //Au chargement du dom
    useEffect(() => {
        //On récupère le client
        getOneEmployee(props.id)
        .then((res) => {
            //S'il est bien récupéré
            if(res.status === 200) {
                //On met à jours la state employee
                setEmployee(res.results)
            //Sinon,
            } else {
                //On ferme le popup et on met à jours le message d'erreur dans le code parent
                props.onClose()
                props.updateMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }, [props])

    return (
        <>
        {employee &&
            <section className="big-popup">
                <h1>{employee.firstName} {employee.lastName.toUpperCase()}</h1>
                <section className="popup-content">
                    <p>Email : {employee.email}</p>
                    <p>Telephone : {employee.phone}</p>
                    <p>Adresse : {employee.addressLineOne}, {employee.postalCode} {employee.city}</p>
                    {employee.addressLineTwo && 
                        <p>Complément d'adresse : {employee.addressLineTwo}</p>
                    }   
                </section>

                <button
                    className="button-white"
                    onClick={(e) => {
                        e.preventDefault()
                        props.onClose()
                    }}
                >Fermer</button>
            </section>
        }
        </>
    )
}