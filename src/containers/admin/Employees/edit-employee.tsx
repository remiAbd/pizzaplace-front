import { useState, useEffect } from "react";
import { getOneEmployee, editOneEmployee } from "../../../api/user-api";

export default function EditEmployee(props : any) {
    const [firstName, setFirstName] = useState<string>("");
    const [lastName, setLastName] = useState<string>("");
    const [email, setEmail] = useState<string>("");
    const [phone, setPhone] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [addressLineOne, setAddressLineOne] = useState<string>("");
    const [addressLineTwo, setAddressLineTwo] = useState<string>("");
    const [postalCode, setPostalCode] = useState<string>("");
    const [city, setCity] = useState<string>("");
    const [role, setRole] = useState <string> ("")
    const [message, setMessage] = useState <string> ("")

    useEffect(() => {
        console.log(props.id)
        getOneEmployee(props.id)
        .then((res) => {
            if(res.status === 200) {
                setFirstName(res.results.firstName);
                setLastName(res.results.lastName);
                setEmail(res.results.email);
                setPhone(res.results.phone);
                setAddressLineOne(res.results.addressLineOne);
                setAddressLineTwo(res.results.addressLineTwo);
                setPostalCode(res.results.postalCode);
                setCity(res.results.city);
                setRole(res.results.role)
            } else {
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }, [])

    //Fonction de gestion de formulaire
    function handleSubmit(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault()
        //On construit l'objet d'envoi
        let datas: {
            firstName: string;
            lastName: string;
            email: string;
            phone: string;
            password: string;
            addressLineOne: string;
            addressLineTwo: string;
            postalCode: string;
            city: string;
            role: string;
        } = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            phone: phone,
            password: password,
            addressLineOne: addressLineOne,
            addressLineTwo: addressLineTwo,
            postalCode: postalCode,
            city: city,
            role: role,
        };
        editOneEmployee(datas, props.id)
        .then((res) => {
            if(res.status === 200) {
                props.updateMessage(res.msg)
                props.onClose()
            } else {
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }

    return (
        <form className="classic-form big-popup">
            <h1>Modifier un employé</h1>
            {message && <p>{message}</p>}

            <label htmlFor="firstName">Prénom:</label>
            <input
                id="firstName"
                type="text"
                name="firstName"
                placeholder="Prénom"
                defaultValue={firstName}
                onChange={(e) => {
                    setFirstName(e.currentTarget.value);
                }}
            />

            <label htmlFor="lastName">Nom:</label>
            <input
                id="lastName"
                type="text"
                name="lastName"
                placeholder="Nom"
                defaultValue={lastName}
                onChange={(e) => {
                    setLastName(e.currentTarget.value);
                }}
            />

            <label htmlFor="email">Email:</label>
            <input
                id="email"
                type="text"
                name="email"
                placeholder="Email"
                defaultValue={email}
                onChange={(e) => {
                    setEmail(e.currentTarget.value);
                }}
            />

            <label htmlFor="phone">Téléphone:</label>
            <input
                id="phone"
                type="text"
                name="phone"
                placeholder="Téléphone"
                defaultValue={phone}
                onChange={(e) => {
                    setPhone(e.currentTarget.value);
                }}
            />

            <label htmlFor="password">Mot de passe:</label>
            <input
                id="password"
                type="text"
                name="password"
                placeholder="Mot de passe"
                defaultValue={password}
                onChange={(e) => {
                    setPassword(e.currentTarget.value);
                }}
            />

            <label htmlFor="address1">Adresse:</label>
            <input
                id="address1"
                type="text"
                name="address1"
                placeholder="Adresse"
                defaultValue={addressLineOne}
                onChange={(e) => {
                    setAddressLineOne(e.currentTarget.value);
                }}
            />

            <label htmlFor="address2">Complément d'adresse:</label>
            <input
                id="address2"
                type="text"
                name="address2"
                placeholder="Complément d'adresse"
                defaultValue={addressLineTwo}
                onChange={(e) => {
                    setAddressLineTwo(e.currentTarget.value);
                }}
            />

            <label htmlFor="postalCode">Code postal:</label>
            <input
                id="postalCode"
                type="text"
                name="postalCode"
                placeholder="Code postal"
                defaultValue={postalCode}
                onChange={(e) => {
                    setPostalCode(e.currentTarget.value);
                }}
            />

            <label htmlFor="city">Ville:</label>
            <input
                id="city"
                type="text"
                name="city"
                placeholder="Ville"
                defaultValue={city}
                onChange={(e) => {
                    setCity(e.currentTarget.value);
                }}
            />

            <label htmlFor="role">Role :</label>
            {role === "classic" && 
            <select
                id="role"
                name="Role"
                onChange={(e) => {
                    e.preventDefault()
                    setRole(e.currentTarget.value)
                }}
            >
                <option selected value="classic">Classique</option>
                <option value="delivery">Livreur</option>
                <option value="admin">Administrateur</option>
            </select>
            }
            {role === "delivery" && 
            <select
                id="role"
                name="Role"
                onChange={(e) => {
                    e.preventDefault()
                    setRole(e.currentTarget.value)
                }}
            >
                <option value="classic">Classique</option>
                <option selected value="Delivery">Livreur</option>
                <option value="admin">Administrateur</option>
            </select>
            }
            {role === "admin" && 
            <select
                id="role"
                name="Role"
                onChange={(e) => {
                    e.preventDefault()
                    setRole(e.currentTarget.value)
                }}
            >
                <option value="classic">Classique</option>
                <option value="Delivery">Livreur</option>
                <option selected value="admin">Administrateur</option>
            </select>
            }

            <div className="button-box">
                <button
                    className="button-white two"
                    onClick={e => handleSubmit(e)}
                >
                    Valider
                </button>
                <button
                    className="button-white two"
                    onClick={(e) => {
                        e.preventDefault();
                        props.onClose();
                    }}
                >
                    Annuler
                </button>
            </div>
        </form>
    )
}   