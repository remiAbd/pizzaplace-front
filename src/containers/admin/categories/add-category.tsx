import { useState } from "react"
import { saveOneCategory } from "../../../api/products-api"




export default function AddCategory(props : any) {
    const [name , setName] = useState <string> ("")
    const [description, setDescription] = useState <string> ("")
    const [message, setMessage] = useState <string> ("")

    //Fonction d'envoi du formulaire
    function handleSubmit() {
        //si les champs sont remplis
        if (name && description) {
            //On construit l'objet à envoyer
            let datas : {
                name : string,
                description : string
            } = {
                name : name, 
                description : description
            }
            //On enregistre la catégorie
            saveOneCategory(datas)
            .then((res) => {
                //si la catégorie est bien enregistrée
                if(res.status === 200) {
                    //On met à jours le message d'erreur dans le code parent
                    props.updateMessage(res.msg)
                    //On ferme le popup
                    props.onClose()
                //sinon
                } else {
                    //On met à jours le message d'erreur
                    setMessage(res.msg)
                }
            })
            .catch(err => console.log(err))
        //sinon,
        } else {
            //On met à jours le message d'erreur
            setMessage("Veuillez remplir tous les champs svp")
        }
    }
    
    return (
    <form className="classic-form big-popup">
        <h1>Ajouter une catégorie</h1>
        {message && <p>{message}</p>}
        <label htmlFor="name">Nom</label>
        <input
            id="name"
            type="text"
            placeholder="Nom"
            onChange={(e) => {
                setName(e.currentTarget.value)
            }}
        />
        <label htmlFor="description">Description</label>
        <input
            id="description"
            type="text"
            placeholder="Description"
            onChange={(e) => {
                setDescription(e.currentTarget.value)
            }}
        />
        <div className="button-box">
            <button
                className="two button-white"
                onClick={(e) => {
                    e.preventDefault()
                    handleSubmit()
                }}
            >Valider</button>
            <button
                className="two button-white"
                onClick={(e) => {
                    e.preventDefault()
                    props.onClose()
                }}
            >Annuler</button>
        </div>
    </form>
    )
}