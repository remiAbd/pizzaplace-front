import { useEffect, useState } from "react"
import { editOneCategory, getOneCategory } from "../../../api/products-api"


export default function EditCategory(props : {id : number, onClose : Function, updateMessage : Function}) {
    const [name, setName] = useState <string> ("")
    const [description, setDescription] = useState <string> ("")
    const [message, setMessage] = useState <string> ("")
    
    //Au chargement des props
    useEffect(() => {
        //On récupère la catégorie
        getOneCategory(props.id)
        .then((res) => {
            //si on la récupère bien
            if (res.status === 200) {
                //On actualise les props avec les informations du backend
                setName(res.results.name)
                setDescription(res.results.description)
            //Sinon
            } else {
                //On actualise le message d'erreur
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }, [])

    //Fonction d'envoi de formulaire
    function handleSubmit() {
        //Si les champs du formulaire sont remplis
        if (description && name) {
            //on construit l'objet d'erreur
            let datas : {
                name : string,
                description : string
            } = {
                name : name, 
                description : description
            }
            //On modifie la catégorie
            editOneCategory(datas, props.id)
            .then((res) => {
                //Si elle est bien modifiée
                if (res.status === 200) {
                    //On acutalise le message d'erreur dans le code parent
                    props.updateMessage(res.msg)
                    //on ferme la popup
                    props.onClose()
                //Sinon
                } else {
                    //On actualise le message d'erreur
                    setMessage(res.msg)
                }
            })
            .catch(err => console.log(err))
        //Sinon,
        } else {
            //On actualise le message d'erreur
            setMessage("Veuillez remplir tous les champs svp")
        }
    }

    return (
    <form className="classic-form big-popup">
    <h3>Modification</h3>
    {message && <p>{message}</p>}
        <label htmlFor="name">Nom</label>
        <input
            id="name"
            type="text"
            placeholder="Nom"
            defaultValue={name}
            onChange={(e) => {
                setName(e.currentTarget.value)
            }}
        />
        <label htmlFor="description">Description</label>
        <input
            id="description"
            type="text"
            defaultValue={description}
            placeholder="description"
            onChange={(e) => {
                setDescription(e.currentTarget.value)
            }}
        />
        <div className="button-box">
            <button
                className="two button-white"
                onClick={(e) => {
                    e.preventDefault()
                    handleSubmit()
                }}
            >Valider</button>
            <button
                className="two button-white"
                onClick={(e) => {
                    e.preventDefault()
                    props.onClose()
                }}
            >Annuler</button>
        </div>
    </form>
    )
}