import {useEffect, useState} from "react"
import { deleteOneCategory, getAllCategories } from "../../../api/products-api"
import Popup from "../../../components/popup/popup"

import EditCategory from "./edit-category"
import AddCategory from "./add-category"
import { useDispatch } from "react-redux"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPen, faXmark } from "@fortawesome/free-solid-svg-icons"
import { changeCategories } from "../../../slices/categories-slices"


export default function CategoriesList(props : any) {
    const [showAddCategory, setShowAddCategory] = useState <Boolean> (false)
    const [showEditCategory, setShowEditCategory] = useState <Boolean> (false)
    const [message, setMessage] = useState <string> ("")
    const [id, setId] = useState <number> (0)
    const [categories, setCategories] = useState <any> ([])
    const dispatch = useDispatch()

    //Au chargement du dom et des props
    useEffect(() => {
        //On met à jours la state catégories avec les props
        setCategories(props.categories)
    }, [props])

    //Fonction de fermeture des popup
    function closePopup() {
        setShowAddCategory(false)
        setShowEditCategory(false)
        refreshCategories()
    }

    //Fonction d'acutalisation du message d'erreur
    function updateMessage(message : string) {
        setMessage(message)
    }

    //Fonction de rafraichissement des catégories
    function refreshCategories() {
        //On récupère les catégories
        getAllCategories()
        .then((res) => {
            //On met à jours la state et la variable redux avec les catégories récupérées
            setCategories(res.results)
            dispatch(changeCategories(res.results))
        })
        .catch(err => console.log(err))
    }

    //Fonction de suppression d'une catégorie
    function deleteOneCat(id : number) {
        //on supprime une catégorie
        deleteOneCategory(id)
        .then((res) => {
            //On met à jours le message d'erreur
            setMessage(res.msg)
            //si la suppression a fonctionnée
            if(res.status === 200) {
                //On rafraichit la liste des catégories
                refreshCategories()
            }
        })
        .catch((err) => console.log(err))
    }
    
    return(
        <>
            <button
                className="button-white add-button button-right"
                onClick={(e) => {
                    e.preventDefault()
                    setShowAddCategory(true)
                }}
            >Ajouter une catégorie</button>

            {message && <p className="status-message">{message}</p>}
            {categories.length > 0 &&
                <table>
                    <thead>
                        <tr>
                            <td>Nom</td>
                            <td>Description</td>
                            <td>Boutons</td>
                        </tr>
                    </thead>
                    <tbody>
                        {categories.map((category : any) => {
                            return(
                                <tr key={category.id}>
                                    <td>{category.name}</td>
                                    <td>{category.description}</td>
                                    <td>
                                        <button
                                            className="edit-button param-button"
                                            onClick={(e) => {
                                                e.preventDefault()
                                                setShowEditCategory(true)
                                                setId(category.id)
                                            }}
                                        ><FontAwesomeIcon icon={faPen}/></button>
                                        <button
                                            className="delete-button param-button"
                                            onClick={(e) => {
                                                e.preventDefault()
                                                deleteOneCat(category.id)
                                            }}
                                        ><FontAwesomeIcon icon={faXmark}/></button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            }
            {showEditCategory && 
                <EditCategory 
                    updateMessage={updateMessage}
                    onClose={closePopup}
                    id = {id}
                />
            }
            {showAddCategory &&
                <AddCategory
                    updateMessage={updateMessage}
                    onClose={closePopup}
                    id = {id}
                />
            }
        </>
    )
    
}