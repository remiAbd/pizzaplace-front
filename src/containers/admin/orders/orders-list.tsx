import { useState, useEffect } from "react"
import { changeStatus, getAllOrders, setShipped } from "../../../api/orders-api"
import { changeOrders } from "../../../slices/orders-slices"
import OrderDetails from "./order-details"
import moment from "moment"
import { useDispatch } from "react-redux"

export default function OrderList(props : any) {
    const [orders, setOrders] = useState <any> ([])
    const [showOrderDetails, setShowOrderDetails] = useState <boolean> (false)
    const [newStatus, setNewStatus] = useState <string> ("")
    const [idToModify, setIdToModify] = useState <number> (0)
    const [id, setId] = useState <number> (0)
    const [message, setMessage] = useState <string> ("")
    const dispatch = useDispatch()

    //Au chargement du dom
    useEffect(() => {
        //On met à jours la state orders
        setOrders(props.orders)
    }, [])

    //FOnction de fermeture du popup
    function closePopup() {
        setShowOrderDetails(false)
        refreshOrders()
    }

    //Fonction de changement de message d'erreur
    function updateMessage(message : string) {
        setMessage(message)
    }

    //Fonction de raffraichissement des commandes pour actualiser l'affichage
    function refreshOrders() {
        //On récupère toutes les commandes du backend
        getAllOrders()
        .then((res) => {
            //On met à jours la state des commandes
            setOrders(res.results)
            //On met à jours la variable redux des commandes
            dispatch(changeOrders(res.results))
        })
        .catch(err => console.log(err))
    }

    //Fonction de changement de status des commandes
    function handleChangeStatus(id : number, status : string) {
        //Si le nouveau status est "shipped"
        if (status === "shipped") {
            //On met à jours la commande
            setShipped(id)
            .then((res) => {
                console.log(res)
                //On met à jours le message d'erreur
                setMessage(res.msg)
            })
            .catch(err => console.log(err))
        //Sinon,
        } else {
            //On change le status de la commande avec le nouveau status
            changeStatus(id, {status : status})
            .then((res) => {
                console.log(res)
                //Obn met à jours le message d'erreur
                setMessage(res.msg)
            })
            .catch(err => console.log(err))
        }
    }

    return (
        <>
            {message && <p className="status-message">{message}</p>}
            {orders.length > 0 && 
                <table>
                    <thead>
                        <tr>
                            <td>Id</td>
                            <td className="small">Date de commande</td>
                            <td className="small">Adresse</td>
                            <td className="small">Telephone</td>
                            <td>Status</td>
                            <td>Amount</td>
                        </tr>
                    </thead>
                    <tbody>
                        {orders.map((order : any) => {
                            return(
                                <tr key={order.ordersID}>
                                    <td>
                                        <button
                                            className="invisible-button"
                                            onClick={(e) => {
                                                e.preventDefault()
                                                setId(order.ordersID)
                                                setShowOrderDetails(!showOrderDetails)
                                            }}
                                        >{order.ordersID}</button>
                                    </td>
                                    <td className="small">{moment(order.orderDate).format("DD/MM/YYYY")}</td>
                                    {order.addressLineOne ? 
                                    <td className="small">{order.addressLineOne}, {order.postalCode} {order.city}</td>
                                    :
                                    <td className="small">À emporter</td>
                                    }
                                    {order.phone ? 
                                    <td className="small">{order.phone}</td>
                                    :
                                    <td className="small">Null</td>}
                                    <td>
                                        <select
                                            onChange={(e) => {
                                                e.preventDefault()
                                                setNewStatus(e.currentTarget.value)
                                                setIdToModify(order.ordersID)
                                                console.log(order.ordersID)
                                            }}                                        
                                        >
                                            {order.status.toLowerCase() === "waiting for payment" && <>
                                                <option value="waiting for payment">waiting for paiment</option>
                                                <option value="paid">Paid</option>
                                                <option value="canceled">Canceled</option>
                                                <option value="shipped">Shipped</option>
                                            </>}
                                            {order.status.toLowerCase() === "paid" && <>
                                                <option value="paid">Paid</option>
                                                <option value="waiting for payment">waiting for paiment</option>
                                                <option value="canceled">Canceled</option>
                                                <option value="shipped">Shipped</option>
                                            </>}
                                            {order.status.toLowerCase() === "canceled" && <>
                                                <option value="canceled">Canceled</option>
                                                <option value="waiting for payment">waiting for paiment</option>
                                                <option value="paid">Paid</option>
                                                <option value="shipped">Shipped</option>
                                            </>}
                                            {order.status.toLowerCase() === "shipped" && <>
                                                <option value="shipped">Shipped</option>
                                                <option value="waiting for payment">waiting for paiment</option>
                                                <option value="paid">Paid</option>
                                                <option value="canceled">Canceled</option>
                                            </>}
                                        </select>
                                        {newStatus !== "" && newStatus !== order.status && idToModify === order.ordersID &&
                                            <button
                                                className="button-white"
                                                onClick={(e)=> {
                                                    e.preventDefault()
                                                    handleChangeStatus(order.ordersID, newStatus)
                                                }}
                                            >save</button>
                                        }
                                    </td>
                                    <td>{order.amount}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            }
            {showOrderDetails && 
                <OrderDetails
                    updateMessage={updateMessage}
                    onClose={closePopup}
                    id = {id}
                />
            }
        </>
    )
}