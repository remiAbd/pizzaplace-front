import { useState, useEffect} from "react"
import { getFullDetails, getOneOrder } from "../../../api/orders-api"
import { getOneCustomer } from "../../../api/user-api"
import moment from "moment"


export default function OrderDetails(props : any) {
    const [order, setOrder] = useState <any> ({})
    const [orderDetails, setOrderDetails] = useState <any> ([])
    const [customer, setCustomer] = useState <any> ({})

    //Au chargement du dom
    useEffect(() => {
        //On récupère la commande
        getOneOrder(props.id)
        .then((res) => {
            //On met à jours la state order avec la commande
            setOrder(res.results)
            //On récupère les détails de la commande
            getFullDetails(props.id)
            .then((response) => {
                //On met à jours la state orderDetails avec la réponse du backend
                setOrderDetails(response.results)
                //Si la commande a été effectuée par un client (i.e qu'il y a un champ customersID dans la commande)
                if (res.results.customersID) {
                    //On récupère le client
                    getOneCustomer(res.results.customersID)
                    .then((responseCustomer) => {
                        //On met à jours la state customer avec le client récupéré du backend
                        setCustomer(responseCustomer.results)
                    })
                    .catch(err => console.log(err))
                }
            })
            .catch(err => console.log(err))
        })
        .catch(err => console.log(err))
    }, [props])

    
    return (
        <section className="big-popup">
            <h1>Commande n°{props.id}</h1>
            <section className="popup-content">
                <p>Date de la commande : {moment(order.orderDate).format("DD-MM-YYYY")}</p>
                {order.addressLineOne ? 
                    <p>Adresse de livraison : {order.addressLineOne}, {order.postalCode} {order.city}</p> : <p>Commande à emporter</p>
                }
                {order.phone && 
                <p>Telephone : {order.phone}</p>}
                <p>Status : {order.status}</p>
                {order.status === "shipped" && order.addressLineOne &&
                <p>Date de livraison : {order.shippedDate}</p>}
                
                {order.comment &&
                <p>Commentaire : {order.comment}</p>}
            
            <h3>Détail de la commande : </h3>
            <ul>
                {orderDetails.map((detail : any) => {
                    return (<li key={detail.ID}>{detail.quantity} * {detail.name} ({detail.price * detail.quantity}€)</li>)
                })}
            </ul>
            <p>Montant total : {order.amount}€</p>
            </section>
            <button
                className="button-white"
                onClick={(e) => {
                    e.preventDefault()
                    props.onClose()
                }}
            >Fermer</button>
        </section>
    )
}