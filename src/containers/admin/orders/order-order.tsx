import { useEffect, useState } from "react"
import { changeStatus, getFullDetails, getOneOrder, getOrderDetails, setShipped } from "../../../api/orders-api"
import { Navigate, useLocation } from "react-router-dom"
import { getOneCustomer } from "../../../api/user-api"
import { getRestaurant } from "../../../api/restaurant-api"
import MapContainer from "../../../components/map"


export default function Order(props : any) {
    const { state } = useLocation()
    const [message, setMessage] = useState <string> ("")
    const [order, setOrder] = useState <any> (null)
    const [orderDetails, setOrderDetails] = useState <any> (null)
    const [customer, setCustomer] = useState <any> (null)
    const [restaurant, setRestaurant] = useState <any> (null)
    const [orderStatus, setOrderStatus] = useState <string> ("")
    const [redirect, setRedirect] = useState <boolean> (false)

    useEffect(() => {
        //On définit le title de la page
        document.title= "Pizzaplace | Commande"
        setRedirect(false)
        if(!state || state === null) {
            setRedirect(true)
        }
        console.log("state : ", state)
        getOneOrder(state)
        .then((res) => {
            console.log(res)
            if(res.status === 200) {
                setOrder(res.results)
                getFullDetails(state)
                .then((resDetails) => {
                    console.log("resdetails", resDetails)
                    if(resDetails.status === 200) {
                        setOrderDetails(resDetails.results)
                        if(order.customersID) {
                            getOneCustomer(order.customersID)
                            .then((resCustomer) => {
                                if(resCustomer.status === 200) {
                                    setCustomer(resCustomer.results)
                                } else {
                                    setMessage(resCustomer.msg)
                                }
                            })
                            .catch(err => console.log(err))
                        }
                    } else {
                        setMessage(resDetails.msg)
                    }
                })
                .catch(err => console.log(err))
            } else {
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
        getRestaurant()
        .then((res) => {
            //Si on récupère bien le restaurant
            if(res.status === 200) {
                //On stock les informations du restaurant dans la state restaurant
                setRestaurant(res.results)
            //Si on ne récupère pas le restaurant
            } else {
                //On met à jours l'erreur
                setMessage(res.msg)
            }
        })
        .catch((err) => console.log(err))
    }, [])

    function updateStatus(e : React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault()
        if (orderStatus === "shipped") {
            setShipped(order.ordersID)
            .then((res) => {
                setMessage(res.msg)
            })
            .catch(err => console.log(err))
        } else {
            changeStatus(order.ordersID, {status : orderStatus})
            .then((res) => {
                setMessage(res.msg)
            })
            .catch(err => console.log(err))
        }
    }

    if(redirect) {
        return <Navigate to="/"/>
    }

    console.log("order : ", order)
    console.log("orderDetails : ", orderDetails)

    return(
        <main className="order-order">
            <div className="title-box">
                <h1>Commande {state.ordersID}</h1>
                {message && <p className="status-message">{message}</p>}
            </div>
            {order && orderDetails && <>
                <section className="contenu">
                    <h2>Contenu de la commande</h2>
                    <ul>
                        {orderDetails &&
                            orderDetails.map((detail : any) => {
                                return(
                                    <li key={detail.ID}>{detail.name} x {detail.quantity}</li>
                                )
                            })
                        }
                    </ul>
                    </section>
                {order.addressLineOne ? 
                    <section className="adresse">
                        <h2>Addresse de livraison</h2>
                        <p>{order.addressLineOne}, {order.postalCode} {order.city}</p>
                        {order.addressLineTwo && <p>Complément d'adresse : {order.addressLineTwo}</p>}
                        <p>{order.phone}</p>
                        {order.latitude && restaurant.latitude &&
                        <MapContainer
                            markerPosition={{lat : order.latitude, lng : order.longitude}}
                            initial={{lat : restaurant.latitude, lng : restaurant.longitude}}
                        />
                        }
                    </section>
                :
                    <section className="adresse">
                        <h2>Commande à emporter</h2>
                        {order && <p>{order.phone}</p>}
                    </section>
                }
                <section className="status">
                    <div className="horizontal">
                        {orderStatus === "waiting for payment" ?
                            <button
                                className="button-white-selected"
                                onClick={(e)=> {
                                    e.preventDefault()
                                    setOrderStatus("waiting for payment")
                                }}
                            >En attente</button>
                        :
                            <button
                                className="button-white"
                                onClick={(e)=> {
                                    e.preventDefault()
                                    setOrderStatus("waiting for payment")
                                }}
                            >En attente</button>
                        }
                        {orderStatus === "paid" ?
                            <button
                                className="button-white-selected"
                                onClick={(e)=> {
                                    e.preventDefault()
                                    setOrderStatus("paid")
                                }}
                            >Payée</button>
                        :
                            <button
                                className="button-white"
                                onClick={(e)=> {
                                    e.preventDefault()
                                    setOrderStatus("paid")
                                }}
                            >Payée</button>
                        }
                        
                        {orderStatus === "canceled" ?
                            <button
                                className="button-white-selected"
                                onClick={(e)=> {
                                    e.preventDefault()
                                    setOrderStatus("canceled")
                                }}
                            >annulée</button>
                        :
                            <button
                                className="button-white"
                                onClick={(e)=> {
                                    e.preventDefault()
                                    setOrderStatus("canceled")
                                }}
                            >Annulée</button>
                        }
                        
                        {orderStatus === "shipped" ?
                            <button
                                className="button-white-selected"
                                onClick={(e)=> {
                                    e.preventDefault()
                                    setOrderStatus("shipped")
                                }}
                            >Livrée</button>
                        :
                            <button
                                className="button-white"
                                onClick={(e)=> {
                                    e.preventDefault()
                                    setOrderStatus("shipped")
                                }}
                            >Livrée</button>
                        }
                    </div>
                    <div className="validate">
                    <button
                        className="button-white"
                        onClick={updateStatus}
                    >Valider</button>
                    </div>
                </section>
            </>} 
        </main>
    )
} 