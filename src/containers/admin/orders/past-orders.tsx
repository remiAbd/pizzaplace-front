import { useState, useEffect } from "react";
import { getAllPastOrders } from "../../../api/orders-api";
// import { useDispatch } from "react-redux";
// import { changeOrders, selectOrders } from "../../slices/orders-slices";
import moment from "moment";
import { Link } from "react-router-dom";

export default function PastOrders(props : any) {
    const [orders, setOrders] = useState <any> (null)
    const [message, setMessage] = useState <string> ("")
    // const dispatch = useDispatch()

    useEffect(() => {
        //On définit le title de la page
        document.title= "Pizzaplace | Commandes passées"
        setMessage("")
        refreshOrders()
    }, [])

    //Fonction de raffraichissement des commandes pour actualiser l'affichage
    function refreshOrders() {
        //On récupère toutes les commandes en cours du backend
        getAllPastOrders()
        .then((res) => {
            //On met à jours la state des commandes
            if(res.status === 200){
                setOrders(res.results)
            } else {
                setMessage(res.msg)
            }
            //On met à jours la variable redux des commandes
            // dispatch(changeOrders(res.results))
        })
        .catch(err => console.log(err))
    }

    return(
        <main className="orders">
            <div className="title-box">
                <h1>Commandes en cours</h1>
            </div>
            {orders &&
            <section>
                {message && <p className="status-message">{message}</p>}
                <table>
                    <thead>
                        <tr>
                            <td>Id</td>
                            <td>Date de commande</td>
                            <td>Adresse</td>
                            <td>Telephone</td>
                            <td>Status</td>
                            <td>Amount</td>
                        </tr>
                    </thead>
                    <tbody>
                    {orders.map((order : any) => {
                            return(
                                <tr key={order.ordersID}>
                                    <td>
                                        <Link to="/order/order" state={order.ordersID}>{order.ordersID}</Link>
                                    </td>
                                    <td>{moment(order.orderDate).format("DD/MM/YYYY")}</td>
                                    {order.addressLineOne ? 
                                    <td>{order.addressLineOne}, {order.postalCode} {order.city}</td>
                                    :
                                    <td>À emporter</td>
                                    }
                                    {order.phone ? 
                                    <td>{order.phone}</td>
                                    :
                                    <td>Null</td>}
                                    <td>{order.status}</td>
                                    <td>{order.amount}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </section>
            }
        </main>
    )
}