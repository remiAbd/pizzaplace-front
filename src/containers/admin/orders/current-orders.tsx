import { useState, useEffect } from "react";
import { getAllCurrentOrders, setShipped, changeStatus } from "../../../api/orders-api";
// import { useDispatch } from "react-redux";
// import { changeOrders, selectOrders } from "../../slices/orders-slices";
import moment from "moment";
import { Link } from "react-router-dom";

export default function CurrentOrders(props : any) {
    const [orders, setOrders] = useState <any> (null)
    const [message, setMessage] = useState <string> ("")
    const [newStatus, setNewStatus] = useState <string> ("")
    const [idToModify, setIdToModify] = useState <number> (0)
    // const dispatch = useDispatch()

    useEffect(() => {
        //On définit le title de la page
        document.title= "Pizzaplace | Commandes en cours"
        setMessage("")
        refreshOrders()
    }, [])

    //Fonction de raffraichissement des commandes pour actualiser l'affichage
    function refreshOrders() {
        //On récupère toutes les commandes en cours du backend
        getAllCurrentOrders()
        .then((res) => {
            //On met à jours la state des commandes
            if(res.status === 200){
                setOrders(res.results)
            } else {
                setMessage(res.msg)
            }
            //On met à jours la variable redux des commandes
            // dispatch(changeOrders(res.results))
        })
        .catch(err => console.log(err))
    }

    //Fonction de changement de status des commandes
    function handleChangeStatus(id : number, status : string) {
        //Si le nouveau status est "shipped"
        if (status === "shipped") {
            //On met à jours la commande
            setShipped(id)
            .then((res) => {
                //On met à jours le message d'erreur
                setMessage(res.msg)
            })
            .catch(err => console.log(err))
        //Sinon,
        } else {
            //On change le status de la commande avec le nouveau status
            changeStatus(id, {status : status})
            .then((res) => {
                //Obn met à jours le message d'erreur
                setMessage(res.msg)
            })
            .catch(err => console.log(err))
        }
        refreshOrders()
    }

    return(
        <main className="orders">
            <div className="title-box">
                <h1>Commandes en cours</h1>
            </div>
            {orders &&
            <section>
                {message && <p className="status-message">{message}</p>}
                <table>
                    <thead>
                        <tr>
                            <td>Id</td>
                            <td className="small">Date de commande</td>
                            <td className="small">Adresse</td>
                            <td className="medium">Telephone</td>
                            <td>Status</td>
                            <td className="medium">Amount</td>
                        </tr>
                    </thead>
                    <tbody>
                    {orders.map((order : any) => {
                            return(
                                <tr key={order.ordersID}>
                                    <td>
                                        <Link to="/order/order" state={order.ordersID}>{order.ordersID}</Link>
                                    </td>
                                    <td className="small">{moment(order.orderDate).format("DD/MM/YYYY")}</td>
                                    {order.addressLineOne ? 
                                    <td className="small">{order.addressLineOne + ", " + order.postalCode + order.city}</td>
                                    :
                                    <td className="small">À emporter</td>
                                    }
                                    {order.phone ? 
                                    <td className="medium">{order.phone}</td>
                                    :
                                    <td className="medium">Null</td>}
                                    <td>
                                        <select
                                            onChange={(e) => {
                                                e.preventDefault()
                                                setNewStatus(e.currentTarget.value)
                                                setIdToModify(order.ordersID)
                                                console.log(order.ordersID)
                                            }}                                        
                                        >
                                            {order.status.toLowerCase() === "waiting for payment" && <>
                                                <option value="waiting for payment">waiting for paiment</option>
                                                <option value="paid">Paid</option>
                                                <option value="canceled">Canceled</option>
                                                <option value="shipped">Shipped</option>
                                            </>}
                                            {order.status.toLowerCase() === "paid" && <>
                                                <option value="paid">Paid</option>
                                                <option value="waiting for payment">waiting for paiment</option>
                                                <option value="canceled">Canceled</option>
                                                <option value="shipped">Shipped</option>
                                            </>}
                                            {order.status.toLowerCase() === "canceled" && <>
                                                <option value="canceled">Canceled</option>
                                                <option value="waiting for payment">waiting for paiment</option>
                                                <option value="paid">Paid</option>
                                                <option value="shipped">Shipped</option>
                                            </>}
                                            {order.status.toLowerCase() === "shipped" && <>
                                                <option value="shipped">Shipped</option>
                                                <option value="waiting for payment">waiting for paiment</option>
                                                <option value="paid">Paid</option>
                                                <option value="canceled">Canceled</option>
                                            </>}
                                        </select>
                                        {newStatus !== "" && newStatus !== order.status && idToModify === order.ordersID &&
                                            <button
                                                className="button-white"
                                                onClick={(e)=> {
                                                    e.preventDefault()
                                                    handleChangeStatus(props.ordersID, newStatus)
                                                }}
                                            >save</button>
                                        }
                                    </td>
                                    <td className="medium">{order.amount}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </section>
            }
        </main>
    )
}