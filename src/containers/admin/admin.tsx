import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { selectProducts } from "../../slices/products-slices";
import { selectCategories } from "../../slices/categories-slices";
import { getAllCustomers, getAllEmployees } from "../../api/user-api";
import { getAllOrders } from "../../api/orders-api";

import CategoriesList from "./categories/categories-list";
import ProductsList from "./products/products-list";
import CustomerList from "./customers/customers-list";
import OrderList from "./orders/orders-list"
import EmployeeList from "./Employees/employee-list";


export default function Admin() {
    //State de gestion d'affichage
    const [showProducts, setShowProducts] = useState <boolean> (true);
    const [showCommands, setShowCommands] = useState <boolean> (false);
    const [showCustomers, setShowCustomers] = useState <boolean> (false);
    const [showCategories, setShowCategories] = useState <boolean> (false);
    const [showEmployees, setShowEmployees] = useState <boolean> (false)
    //state de gestion de message d'erreur
    const [message, setMessage] = useState <string> ("")
    
    //On récupère les produits et catégories de redux
    const products = useSelector(selectProducts);
    const categories = useSelector(selectCategories);
    const [customers, setCustomers] = useState <any> (null)
    const [orders, setOrders] = useState <any> (null)
    const [employees, setEmployees] = useState <any> (null)

    //Au chargement du dom
    useEffect(() => {
        //On définit le title de la page
        document.title= "Pizzaplace | Administration"
        //On set le message d'erreur à vide par défaut
        setMessage("")
        //on récupères tous les clients
        getAllCustomers()
        .then((res) => {
            console.log("res1 : ",res)
            //si on les récupère bien
            if(res.status === 200) {
                //On met à jours la state customers
                setCustomers(res.results)
                //On récupère toutes les commandes
                getAllOrders()
                .then((response) => {
                    console.log("res2 : ",response)
                    //si on les récupère bien
                    if(response.status === 200) {
                        //On met à jours la state orders
                        setOrders(response.results)
                        //On récupère les employés
                        getAllEmployees()
                        .then((resEmployees) => {
                            console.log("resemployés : ", resEmployees)
                            if(resEmployees.status === 200) {
                                setEmployees(resEmployees.results)
                            } else {
                                setMessage(resEmployees.msg)
                            }
                        })
                        .catch(err => console.log(err))
                    //Sinon
                    } else {
                        //On met à jours le message d'erreur avec la réponse du backend
                        setMessage(response.msg)
                    }
                })
                .catch(err => console.log(err))
            //Sinon,
            } else {
                //on met à jours le message d'erreur avec la réponse du backend
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))

    }, [])

    //Fonction de changement d'affichage vers les produits
    function changeToProducts() {
        setShowProducts(true);
        setShowCommands(false);
        setShowCustomers(false);
        setShowCategories(false);
        setShowEmployees(false);
    };

    //Fonction de changement d'affichage vers les commandes
    function changeToOrders() {
        setShowProducts(false);
        setShowCommands(true);
        setShowCustomers(false);
        setShowCategories(false);
        setShowEmployees(false);
    };

    //Fonction de changement d'affichage vers les catégories
    function changeToCategories() {
        setShowProducts(false);
        setShowCommands(false);
        setShowCustomers(false);
        setShowCategories(true);
        setShowEmployees(false);
    };

    //Fonction de changement d'affichage vers les clients
    function changeToCustomers() {
        setShowProducts(false);
        setShowCommands(false);
        setShowCustomers(true);
        setShowCategories(false);
        setShowEmployees(false);
    };

    function changeToEmployees() {
        setShowProducts(false);
        setShowCommands(false);
        setShowCustomers(false);
        setShowCategories(false);
        setShowEmployees(true);
    }

    return(
        <main className="administration">
            <div className="title-box">
                <h1>Admin</h1>
            </div>
            
            {message && <p className="status-message">{message}</p>}
            <section className="button-box">
                <button
                    className="button-white"
                    onClick={(e) => {
                        e.preventDefault();
                        changeToProducts();
                    }}
                >
                    Produits
                </button>

                <button
                    className="button-white"
                    onClick={(e) => {
                        e.preventDefault();
                        changeToCustomers();
                    }}
                >
                    Clients
                </button>

                <button
                    className="button-white"
                    onClick={(e) => {
                        e.preventDefault();
                        changeToOrders();
                    }}
                >
                    Commandes
                </button>

                <button
                    className="button-white"
                    onClick={(e) => {
                        e.preventDefault();
                        changeToCategories();
                    }}
                >
                    Catégories
                </button>

                <button
                    className="button-white"
                    onClick={(e) => {
                        e.preventDefault();
                        changeToEmployees();
                    }}
                >
                    Employés
                </button>
            </section>

            <section className="list-box">
                {showCategories && <CategoriesList categories={categories.categories}/>}
                {showCommands && <OrderList orders={orders}/>}
                {showProducts && <ProductsList products={products.products}/>}
                {showCustomers && <CustomerList customers={customers}/>}
                {showEmployees && <EmployeeList employees={employees}/>}
            </section>
        </main>
    )
}