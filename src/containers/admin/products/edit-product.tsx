import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { selectCategories } from "../../../slices/categories-slices";
import { editOneProduct, saveOneImage } from "../../../api/products-api";
import { getOneProduct } from "../../../api/products-api";



export default function EditProduct(props :any) {
    const [name, setName] = useState <string> ("")
    const [content, setContent] = useState <string> ("")
    const [ingredients, setIngredients] = useState <string> ("")
    const [url, setUrl] = useState <string> ("")
    const [alt, setAlt] = useState <string> ("")
    const [price, setPrice] = useState <number> (0)
    const [categoryId, setCategoryId] = useState <number> (0)
    const [message, setMessage] = useState <string> ("")
    const [image, setImage] = useState <any> ({})
    const categories = useSelector(selectCategories)

    //Au chargement du dom et des props
    useEffect(() => {
        //On récupère les infos d'un produit
        getOneProduct(props.productId)
        .then((res) => {
            //si on les récupère bien
            if(res.status === 200) {
                //On met à jours les states avec les informations du produit
                setName(res.results.name)
                setContent(res.results.content)
                setIngredients(res.results.ingredients)
                setUrl(res.results.url)
                setAlt(res.results.alt)
                setPrice(res.results.price)
                setCategoryId(res.results.categoryId)
            //Sinon
            } else {
                //On met à jours le message d'erreur
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }, [props])
    

    //fonction de gestion de formulaire
    function handleSubmit() {
        //Si le prix est un chiffre
        if (!isNaN(price)) {
            //si tous les champs obligatoires sont remplis
            if (categoryId && name && price && ingredients && content) {
                //si le produit n'a pas d'image et que l'ancien produit n'en avait pas non plus
                if (image === null && url === "no-pict.jpg") {
                    //On construit l'objet d'envoi
                    let datas : {
                        name : string,
                        content : string,
                        ingredients : string,
                        price : number,
                        alt : string,
                        url : string,
                        categoryId : number
                    } = {
                        name : name,
                        content : content,
                        ingredients : ingredients,
                        price : price,
                        alt : "image non disponible",
                        url : "no-pict.jpg",
                        categoryId : categoryId
                    }
                    //On modifie le produit
                    editOneProduct(datas, props.productId)
                    .then((res) => {
                        //si on l'a bien modifié
                        if (res.status === 200) {
                            //on set le message de status vers le composant parent
                            props.updateMessage(res.msg)
                            //On ferme le popup
                            props.onClose()
                        //Sinon
                        } else {
                            //On set le message de status
                            setMessage(res.msg)
                        }
                    })
                    .catch(err => console.log(err))
                //Si le produit garde la même image
                } else if (image !== null && image.name === url) {
                    //On construit l'objet d'envoi
                    let datas : {
                        name : string,
                        content : string,
                        ingredients : string,
                        price : number,
                        alt : string,
                        url : string,
                        categoryId : number
                    } = {
                        name : name,
                        content : content,
                        ingredients : ingredients,
                        price : price,
                        alt : alt,
                        url : url,
                        categoryId : categoryId
                    }
                    //On modifie le produit
                    editOneProduct(datas, props.productId)
                    .then((res) => {
                        //si on l'a bien modifié
                        if (res.status === 200) {
                            //on set le message de status vers le composant parent
                            props.updateMessage(res.msg)
                            //On ferme le popup
                            props.onClose()
                        //Sinon
                        } else {
                            //On set le message de status
                            setMessage(res.msg)
                        }
                    })
                    .catch(err => console.log(err))
                //Si le produit a une nouvelle image
                } else {
                    //On crée un objet FormData qui nous permettera d'envoyer une image vers le backend pour la sauvegarder
                    let formData = new FormData()
                    //On ajoute l'image à notre objet FormData
                    formData.append("image", image)
                    //On sauvegarde l'image
                    saveOneImage(formData)
                    .then((res) => {
                        //si elle a bien été sauvegardée
                        if (res.status === 200) {
                            //On crée l'objet d'envoi
                            let datas : {
                                name : string,
                                content : string,
                                ingredients : string,
                                price : number,
                                alt : string,
                                url : string,
                                categoryId : number
                            } = {
                                name : name,
                                content : content,
                                ingredients : ingredients,
                                price : price,
                                alt : alt,
                                url : res.url,
                                categoryId : categoryId
                            }
                            //On modifie le produit
                            editOneProduct(datas, props.productId)
                            .then((res) => {
                                //s'il a bien été sauvegardé
                                if (res.status === 200) {
                                    //on set le message de status vers le composant parent
                                    props.updateMessage(res.msg)
                                    //On ferme le popup
                                    props.onClose()
                                //Sinon,
                                } else {
                                    //On set le message de status
                                    setMessage(res.msg)
                                }
                            })
                            .catch(err => console.log(err))
                        }
                    })
                    .catch((err) => console.log(err))
                }
            //Sinon
            } else {
                //On met à jours le message d'erreur
                setMessage("Veuillez remplir tous les champs obligatoires s'il vous plait")
            }
        //Sinon
        } else {
            //On met à jours le message d'erreur
            setMessage("Le prix n'est pas un nombre")
        }
    }

    return(
        <form className="classic-form big-popup">
            <h1>Modifier un produit</h1>
            {message && <p>{message}</p>}
            <label htmlFor="name">Nom</label>
            <input
                type="text"
                id="name"
                placeholder="Nom"
                defaultValue={name}
                onChange={(e) => {
                    setName(e.currentTarget.value);
                }}
            />

            <label htmlFor="description">Description</label>
            <input
                type="text"
                id="description"
                placeholder="Description"
                defaultValue={content}
                onChange={(e) => {
                    setContent(e.currentTarget.value);
                }}
            />

            <label htmlFor="ingredients">Ingrédients</label>
            <input
                type="text"
                id="ingredients"
                placeholder="Ingrédients"
                defaultValue={ingredients}
                onChange={(e) => {
                    setIngredients(e.currentTarget.value);
                }}
            />

            <label htmlFor="altText">Texte alternatif de l'image</label>
            <input
                type="text"
                id="altText"
                placeholder="Texte alternatif de l'image"
                defaultValue={alt}
                onChange={(e) => {
                    setAlt(e.currentTarget.value);
                }}
            />

            <label htmlFor="price">Prix du produit</label>
            <input
                type="text"
                id="price"
                placeholder="Prix du produit"
                defaultValue={String(price)}
                onChange={(e) => {
                    setPrice(parseInt(e.currentTarget.value));
                }}
            />

            <label htmlFor="category">Catégorie</label>
            <select
                id="category"
                onChange={(e) => {
                    e.preventDefault();
                    setCategoryId(parseInt(e.currentTarget.value));
                }}
                >
                <option value="">Sélectionnez une catégorie</option>
                {categories.categories.map((category: any) => {
                    return (
                    <option
                        value={category.id}
                        key={category.id}
                        selected={category.id === categoryId}
                    >
                        {category.name}
                    </option>
                    );
                })}
            </select>

            <label htmlFor="image">Image du produit</label>
            <input
                type="file"
                id="image"
                onChange={(e) => {
                    e.preventDefault();
                    setImage(e.currentTarget.files[0]);
                }}
            />

                
            <div className="button-box">
                <button
                    className="two button-white"
                    onClick={(e) => {
                        e.preventDefault()
                        handleSubmit()
                    }}
                >Valider
                </button>
                <button
                    className="two button-white"
                    onClick={(e) => {
                        e.preventDefault()
                        props.onClose()
                    }}
                >Annuler</button>
            </div>
        </form>
    )
}