import { useState } from "react";
import { useSelector } from "react-redux";
import { selectCategories } from "../../../slices/categories-slices";
import { saveOneImage, saveOneProduct } from "../../../api/products-api";



export default function AddProduct(props : any) {
    const [name, setName] = useState <string> ("")
    const [content, setContent] = useState <string> ("")
    const [ingredients, setIngredients] = useState <string> ("")
    const [alt, setAlt] = useState <string> ("")
    const [price, setPrice] = useState <number> (0)
    const [categoryId, setCategoryId] = useState <number> (0)
    const [message, setMessage] = useState <string> ("")
    const [image, setImage] = useState <any | null> (null)
    const categories = useSelector(selectCategories)
    

    //Fonction de gestion de formulaire
    function handleSubmit() {
        //Si le prix est un chiffre
        if(!isNaN(price)) {
            //si tous les champs obligatoires sont remplis
            if(name && content && ingredients && price && categoryId) {
                //si le produit n'a pas d'image
                if(image === null) {
                    //On construit l'objet d'envoi
                    let datas : {
                        name : string,
                        content : string,
                        ingredients : string,
                        price : number,
                        alt : string,
                        url : string,
                        categoryId : number
                    } = {
                        name : name,
                        content : content,
                        ingredients : ingredients,
                        price : price,
                        alt : "image non disponible",
                        url : "no-pict.jpg",
                        categoryId : categoryId
                    }
                    //On sauvegarde le produit
                    saveOneProduct(datas)
                    .then((res) => {
                        //si on l'a bien sauvegardé
                        if (res.status === 200) {
                            //on set le message de status vers le composant parent
                            props.updateMessage(res.msg)
                            //On ferme le popup
                            props.onClose()
                        //Sinon
                        } else {
                            //On set le message de status
                            setMessage(res.msg)
                        }
                    })
                    .catch(err => console.log(err))
                //si le produit a une image
                } else {
                    //On crée un objet FormData qui nous permettera d'envoyer une image vers le backend pour la sauvegarder
                    let formData = new FormData()
                    //On ajoute l'image à notre objet FormData
                    formData.append("image", image)
                    //On sauvegarde l'image
                    saveOneImage(formData)
                    .then((res) => {
                        //si elle a bien été sauvegardée
                        if (res.status === 200) {
                            //On crée l'objet d'envoi
                            let datas : {
                                name : string,
                                content : string,
                                ingredients : string,
                                price : number,
                                alt : string,
                                url : string,
                                categoryId : number
                            } = {
                                name : name,
                                content : content,
                                ingredients : ingredients,
                                price : price,
                                alt : alt,
                                url : res.url,
                                categoryId : categoryId
                            }
                            //On sauvegarde le produit
                            saveOneProduct(datas)
                            .then((res) => {
                                //s'il a bien été sauvegardé
                                if (res.status === 200) {
                                    //on set le message de status vers le composant parent
                                    props.updateMessage(res.msg)
                                    //On ferme le popup
                                    props.onClose()
                                //Sinon,
                                } else {
                                    //On set le message de status
                                    setMessage(res.msg)
                                }
                            })
                            .catch(err => console.log(err))
                        //Sinon
                        } else {
                            //On met à jours le message d'erreur
                            setMessage(res.msg)
                        }
                    })
                    .catch((err) => console.log(err))
                }
            //Sinon
            } else {
                //On met à jours le message d'erreur
                setMessage("Veuillez remplir tous les champs obligatoires s'il vous plait")
            }
        //Sinon
        } else {
            //On met à jours le message d'erreur
            setMessage("Le prix n'est pas un nombre")
        }
    }

    return(
        <form className="classic-form big-popup">
            <h1>Ajouter un produit</h1>
            {message && <p>{message}</p>}
            <label htmlFor="name">Nom</label>
            <input 
                type="text"
                id="name"
                placeholder="Nom"
                onChange={(e) => {
                    setName(e.currentTarget.value)
                }}
            />

            <label htmlFor="description">Description</label>
            <input 
                type="text"
                id="description"
                placeholder="Description"
                onChange={(e) => {
                    setContent(e.currentTarget.value)
                }}
            />

            <label htmlFor="ingredients">Ingrédients</label>
            <input 
                type="text"
                id="ingredients"
                placeholder="Ingrédients"
                onChange={(e) => {
                    setIngredients(e.currentTarget.value)
                }}
            />

            <label htmlFor="imageAlt">Alt de l'image</label>
            <input 
                type="text"
                id="imageAlt"
                placeholder="Alt de l'image"
                onChange={(e) => {
                    setAlt(e.currentTarget.value)
                }}
            />

            <label htmlFor="price">Prix</label>
            <input 
                type="text"
                id="price"
                placeholder="Prix"
                onChange={(e) => {
                    setPrice(parseInt(e.currentTarget.value))
                }}
            />

            <label htmlFor="category">Catégorie</label>
            <select
                id="category"
                onChange={(e) => {
                    e.preventDefault()
                    setCategoryId(parseInt(e.currentTarget.value))
                }}
            >
                <option value="">Selectionnez une catégorie</option>
                {categories.categories.map((category : any) => {
                    return(
                        <option value={category.id} key={category.id}>{category.name}</option>
                    )
                })}
            </select>

            <label htmlFor="file">Selectionnez une image</label>
            <input
                id="file" 
                type="file"
                onChange={(e) => {
                    setImage(e.currentTarget.files[0])
                }}
            />
            <div className="button-box">
                <button
                    className="two button-white"
                    onClick={(e) => {
                        e.preventDefault()
                        handleSubmit()
                    }}
                >Valider
                </button>
                <button
                    className="two button-white"
                    onClick={(e) => {
                        e.preventDefault()
                        props.onClose()
                    }}
                >Annuler</button>
            </div>
        </form>
    )
}