import { useEffect, useState } from "react";
import { getOneCategory, getOneProduct } from "../../../api/products-api";
import { config } from "../../../config";


export default function ShowProduct(props : any) {
    const [name, setName] = useState <string> ("")
    const [content, setContent] = useState <string> ("")
    const [ingredients, setIngredients] = useState <string> ("")
    const [alt, setAlt] = useState <string> ("")
    const [price, setPrice] = useState <number> (0)
    const [category, setCategory] = useState <number> (0)
    const [picture, setPicture] = useState <any> (null)

    //Au chargement du dom,
    useEffect(() =>{
        //On récpère le produit
        getOneProduct(props.productId)
        .then((res) => {
            //Si on le récupère bien
            if (res.status === 200) {
                //On met à jours les states avec les informations du backend
                setName(res.results.name)
                setContent(res.results.content)
                setIngredients(res.results.ingredients)
                setAlt(res.results.alt)
                setPrice(res.results.price)
                setPicture(res.results.url)
                //On récupère les catégories
                getOneCategory(res.results.categoryId)
                .then((response) => {
                    //Si on les récupère bien
                    if (response.status === 200) {
                        //On met à jours la state de la catégorie du produit
                        setCategory(response.results.name)
                    }
                })
                .catch(err => console.log(err))  
            }         
        })
        .catch(err => console.log(err))
    }, [props])

    return (
        <section className="big-popup">
            <div className="title-box">
                <h1>Détails</h1>
            </div>
            <section className="popup-content">
                <div className="basket-picture"><img className="image" alt={alt} src={config.pict_url + picture}/></div>
                <h2>{name}</h2>
                <p>{content}</p>
                <p>Ingrédients : {ingredients}</p>
                <p>Prix : {price} €</p>
            </section>
            <button 
                className="button-white"
                onClick={(e) => {
                    e.preventDefault()
                    props.onClose()
                }}
            >Fermer</button>
        </section>
    )
}