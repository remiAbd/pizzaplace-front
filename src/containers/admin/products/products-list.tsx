import AddProduct from "./add-product"
import EditProduct from "./edit-product"
import ShowProduct from "./show-product";
import {useEffect, useState} from "react"
import { useDispatch } from "react-redux";
import { config } from "../../../config";

import { deleteOneProduct, getAllProducts } from "../../../api/products-api";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPen, faXmark } from "@fortawesome/free-solid-svg-icons"
import { changeProducts } from "../../../slices/products-slices";

export default function ProductsList(props : any) {
    const [showAddProduct, setShowAddProduct] = useState <Boolean> (false)
    const [showEditProduct, setShowEditProduct] = useState <Boolean> (false)
    const [productIdToEdit, setProductIdToEdit] = useState <number> (0)
    const [message, setMessage] = useState <string> ("")
    const [products, setProducts] = useState <any> ([])
    const [showProductDetails, setShowProductDetails] = useState <boolean> (false)
    const dispatch = useDispatch()


    //Au chargement du dom
    useEffect(() => {
        //On met à jours la state products avec la props du code parent
        setProducts(props.products)
    }, [props])

    //Fonction de modification du message d'erreur (envoyé dans les popups)
    function updateMessage(message : string) {
        setMessage(message)
    }

    //Fonction de fermeture des popups
    function closePopup() {
        setShowAddProduct(false)
        setShowEditProduct(false)
        setShowProductDetails(false)
        refreshProducts()
    }

    //fonction qui raffraichit la liste des produits pour la garder à jours
    function refreshProducts() {
        //On récupère tous les produits du backend
        getAllProducts()
        .then((res) => {
            //Si on les récupère bien
            if(res.status === 200) {
                //On met à jours la state des produits et la variable de redux
                setProducts(res.results)
                dispatch(changeProducts(res.results))
            //Sinon,
            } else {
                //on met à jours le message d'erreur
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }

    //fonction de suppression d'un produit
    function deleteProduct(id : number) {
        //Fonction de suppression d'un produit dans le backend
        deleteOneProduct(id)
        .then((res) => {
            //Si le produit a bien été supprimé
            if (res.status === 200) {
                //On raffraichit les produits
                refreshProducts()
            //Sinon
            } else {
                //On met à jours le message d'erreur
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }

    return(
        <>
            {message && <p className="status-message">{message}</p>}
            <button
                className="button-white add-button button-right"
                onClick={() => {
                    setShowAddProduct(!showAddProduct)
                }}
            >Ajouter un produit</button>
            {products.length > 0 && 
                <table>
                    <thead>
                        <tr>
                            <td>Image</td>
                            <td>Nom</td>
                            <td className="small">Description</td>
                            <td className="medium">Ingrédients</td>
                            <td className="medium">Prix</td>
                            <td className="medium">alt</td>
                            <td className="medium">Categorie</td>
                            <td>Boutons</td>
                        </tr>
                    </thead>
                    <tbody>
                        {products.map((product : any) => {
                            return(
                                <tr key={product.id}>
                                    <td className="product-picture">
                                        <div><img className="image" alt={product.alt} src={config.pict_url + product.url}/></div>
                                    </td>
                                    <td>
                                        <button 
                                            className="invisible-button"
                                            onClick={(e) => {
                                                e.preventDefault()
                                                setProductIdToEdit(product.id)
                                                setShowProductDetails(!showProductDetails)
                                            }}
                                        >{product.name}</button>
                                    </td>
                                    <td className="small">{product.content}</td>
                                    <td className="medium">{product.ingredients}</td>
                                    <td className="medium">{product.price}</td>
                                    <td className="medium">{product.alt}</td>
                                    <td className="medium">{product.categoryId}</td>
                                    <td>
                                        <button
                                            className="edit-button param-button"
                                            onClick={(e) => {
                                                e.preventDefault()
                                                setProductIdToEdit(product.id)
                                                setShowEditProduct(!showEditProduct)
                                            }}
                                        ><FontAwesomeIcon icon={faPen}/></button>
                                        <button
                                            className="delete-button param-button"
                                            onClick={(e)=> {
                                                e.preventDefault()
                                                deleteProduct(parseInt(product.id))
                                            }}
                                        ><FontAwesomeIcon icon={faXmark}/></button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            }
            {showAddProduct && 
                <AddProduct 
                    updateMessage={updateMessage}
                    onClose={closePopup}
                />
            }
            {showEditProduct && 
                <EditProduct 
                    updateMessage={updateMessage}
                    onClose={closePopup}
                    productId = {productIdToEdit}
                />
            }
            {showProductDetails &&
                <ShowProduct 
                onClose={closePopup}
                productId = {productIdToEdit}
            />
            }
        </>
    )
}