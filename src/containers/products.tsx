import { useSelector } from "react-redux"
import { selectProducts } from "../slices/products-slices"
import { selectCategories } from "../slices/categories-slices"
import ProductDetails from "./product-details"
import { useEffect } from "react"

export default function Products() {
    //On récupère la variable redux des produits
    const products = useSelector(selectProducts)
    //On récupère la variables redux des catégories
    const categories = useSelector(selectCategories)

    useEffect(() => {
        //On définit le title de la page
        document.title= "Pizzaplace | Carte"
    }, [])

    return(
        <main className="products-menu">
            <div className="title-box">
                <h1>Nos produits</h1>
            </div>
            {products && categories &&
                <section className="categories">
                    {categories.categories.map((category : any, index : number) => {
                        return(<div className="category" key={index}>
                            <h2>{category.name}</h2>
                            <p>{category.description}</p>
                            <div className="products">
                                {products.products.map((product : any, indexP : number) => {
                                    if (product.categoryId === category.id) {
                                        return <ProductDetails product={product} isBasket={false} key={indexP}/>
                                    }
                                })}
                            </div>
                        </div>)
                    })}
                </section>
            }
        </main>
    )
}