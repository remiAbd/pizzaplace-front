import { useSelector } from "react-redux";
import { selectBasket } from "../slices/basket-slices";
import { useEffect, useState } from "react";
import { Link, Navigate } from "react-router-dom";
import { selectUser } from "../slices/user-slices";
import { getOneCustomer } from "../api/user-api";
import { saveOneOrder } from "../api/orders-api";
import ProductDetails from "./product-details";
import { getRestaurant } from "../api/restaurant-api";
import MapContainer from "../components/map"

export default function Basket() {
    const basket: any = useSelector(selectBasket);
    const [livraison, setLivraison] = useState<Boolean>(true);
    const user = useSelector(selectUser);
    const [addressLineOne, setAddressLineOne] = useState<string>("");
    const [addressLineTwo, setAddressLineTwo] = useState<string>("");
    const [postalCode, setPostalCode] = useState<string>("");
    const [city, setCity] = useState<string>("");
    const [phone, setPhone] = useState<string>("");
    const [comment, setComment] = useState<string>("");
    const [redirect, setRedirect] = useState<Boolean>(false);
    const [orderId, setOrderID] = useState<number>(0);
    const [customer, setCustomer] = useState<any>({});
    const [message, setMessage] = useState<string>("");
    const [restaurant, setRestaurant] = useState<any>({});

    //Au chargement du dom et de la variable user venant de redux,
    useEffect(() => {
        //On définit le title de la page
        document.title= "Pizzaplace | Panier"
        //On set la valeurs du lessage d'erreur à vide
        setMessage("");
        //On récupère du backend les informations du restaurant
        getRestaurant()
            .then((response) => {
                //si on les récupère bien
                if (response.status === 200) {
                    //On stock les informations du restaurant dans la state restaurant
                    setRestaurant(response.results);
                    //Sinon
                } else {
                    //On met à jours le message d'erreur avec la réponse du backend
                    setMessage(response.msg);
                }
            })
            .catch((err) => console.log(err));
        //Si l'utilisateur est connecté
        if (user.isLogged) {
            //On récupère du backend ses informations
            getOneCustomer(user.infos.key_id)
            .then((res: any) => {
                //si on les récupères bien
                if (res.status === 200) {
                    //On met à jour la state customer avec ses informations
                    setCustomer(res.results);
                    //Sinon
                }
            })
            .catch((err) => console.log(err));
        }
    }, [user]);

    //Fonction se déclanchant à la validation
    function handleSubmit() {
        //Si l'utilisateur a choisi une commande à emporter
        if (!livraison) {
            //Si l'utilisateur est déconnecté
            if (!user.isLogged) {
                //On crée l'object de commande
                var datas: any = {
                    customersId: null,
                    status: "waiting",
                    comments: comment,
                    phone: "",
                    addressLineOne: "",
                    addressLineTwo: "",
                    postalCode: "",
                    city: "",
                    basket: basket.basket,
                    amount: basket.totalPrice,
                };
                //Si l'utilisateur est connecté
            } else {
                //On crée l'object de commande
                var datas: any = {
                    customersId: customer.customersID,
                    status: "waiting",
                    comments: comment,
                    phone: customer.phone,
                    addressLineOne: "",
                    addressLineTwo: "",
                    postalCode: "",
                    city: "",
                    basket: basket.basket,
                    amount: basket.totalPrice,
                };
            }
            //Si l'utilisateur a choisi la livraison
        } else {
            //Si l'utilisateur est déconnecté
            if (!user.isLogged) {
                //Si tous les chammps obligatoires sont remplis
                if (addressLineOne && postalCode && city && phone) {
                    //On crée l'object de commande
                    var datas: any = {
                        customersId: null,
                        status: "waiting",
                        comments: comment,
                        phone: phone,
                        addressLineOne: addressLineOne,
                        addressLineTwo: addressLineTwo,
                        postalCode: postalCode,
                        city: city,
                        basket: basket.basket,
                        amount: basket.totalPrice,
                    };
                    //Sinon
                } else {
                    //On envoi un message d'erreur à l'utilisateur
                    setMessage(
                        "Veuillez remplir tous les champs s'il vous plait"
                    );
                }
                //S'il est connecté
            } else {
                //On crée l'object de commande
                var datas: any = {
                    customersId: customer.customersID,
                    status: "waiting",
                    comments: comment,
                    phone: customer.phone,
                    addressLineOne: customer.addressLineOne,
                    addressLineTwo: customer.addressLineTwo,
                    postalCode: customer.postalCode,
                    city: customer.city,
                    basket: basket.basket,
                    amount: basket.totalPrice,
                };
            }
        }
        //On sauvegarde la commande dans le backend
        saveOneOrder(datas)
        .then((res) => {
            //Si on sauvegarde bien la commande
            if (res.status === 200) {
                //on récupère l'id de la commande et on autorise la redirection
                setOrderID(res.orderId);
                setRedirect(true);
                //Sinon
            } else {
                //On met à jours le message d'erreur avec la réponse du backend
                console.log(res)
                setMessage(res.msg);
            }
        })
        .catch((err) => console.log(err));
    }

    //Si la redirection est autorisée et qu'on a bien un id de commande
    if (redirect && orderId) {
        //On redirige vers la paiment de la commande
        return <Navigate to="/basket/payment" state={{ orderId: orderId }} />;
    }

    return (
        <main className="basket">
            <div className="title-box">
                <h1>Panier</h1>
                {message && <p className="status-message">{message}</p>}
            </div>
            {basket.basket.length > 0 ? (
                <section className="products-box-basket">
                    <h2>Récapitulatif de la commande</h2>
                    {basket.basket.map((item: any, index : number) => {
                        if (index %2 === 0) {
                            return (
                                <ProductDetails
                                    product={item.product}
                                    quantity={item.quantity}
                                    isBasket={true}
                                    key={item.product.id}
                                    backgroundColor="white"
                                />
                            );
                        } else {
                            return (
                                <ProductDetails
                                    product={item.product}
                                    quantity={item.quantity}
                                    isBasket={true}
                                    key={item.product.id}
                                    backgroundColor="grey"
                                />
                            );
                        }
                    })}
                </section>
            ) : (
                <section className="products-box-basket">
                    <p className="message"><em>Votre panier est vide</em></p>
                </section>
            )}

            <section className="order-type-selection">
                <h2>Choisissez votre mode de livraison</h2>
                <div className="horizontal">
                    {livraison ? <>
                    <button
                        className="button-white"
                        onClick={() => {
                            setLivraison(false);
                        }}
                    >
                        <p>À emporter</p>
                    </button>
                    <button
                        className="button-white-selected"
                        onClick={() => {
                            setLivraison(true);
                        }}
                    >
                        <p>Livraison</p>
                    </button>
                    </> : <>
                    <button
                        className="button-white-selected"
                        onClick={() => {
                            setLivraison(false);
                        }}
                    >
                        <p>À emporter</p>
                    </button>
                    <button
                        className="button-white"
                        onClick={() => {
                            setLivraison(true);
                        }}
                    >
                        <p>Livraison</p>
                    </button>
                    </>}
                </div>
            </section>

            {livraison ? (
                <section className="delivery-address-box">
                    <h2>Addresse de livraison</h2>
                    {user.isLogged && user.infos.role === "customer" && (
                        <>
                            <form>
                                <label htmlFor="address">Votre Adresse</label>
                                <input
                                    id="address"
                                    type="text"
                                    placeholder="Votre addresse"
                                    defaultValue={customer.addressLineOne}
                                    onChange={(e) => {
                                        setAddressLineOne(
                                            e.currentTarget.value
                                        );
                                    }}
                                />
                                <label htmlFor="zip">Code postal</label>
                                <input
                                    id="zip"
                                    type="text"
                                    placeholder="Code postal"
                                    defaultValue={customer.postalCode}
                                    onChange={(e) => {
                                        setPostalCode(e.currentTarget.value);
                                    }}
                                />
                                <label htmlFor="city">Ville</label>
                                <input
                                    id="city"
                                    type="text"
                                    placeholder="Ville"
                                    defaultValue={customer.city}
                                    onChange={(e) => {
                                        setCity(e.currentTarget.value);
                                    }}
                                />
                                <label htmlFor="addressTwo">
                                    Compléments d'adresse
                                </label>
                                <input
                                    id="addressTwo"
                                    type="text"
                                    placeholder="Compléments d'addresse"
                                    defaultValue={customer.addressLineTwo}
                                    onChange={(e) => {
                                        setAddressLineTwo(
                                            e.currentTarget.value
                                        );
                                    }}
                                />
                                <label htmlFor="phone">Téléphone</label>
                                <input
                                    id="phone"
                                    type="text"
                                    placeholder="numéro de téléphone"
                                    defaultValue={customer.phone}
                                    onChange={(e) => {
                                        e.preventDefault();
                                        setPhone(e.currentTarget.value);
                                    }}
                                />
                            </form>
                        </>
                    )}
                    {!user.isLogged && (
                        <>
                            <p>
                                Vous avez un compte ? Connectez vous{" "}
                                <Link to="/login">ici</Link>
                            </p>
                            <form className="classic-form">
                                <label htmlFor="address">Votre Adresse</label>
                                <input
                                    id="adress"
                                    type="text"
                                    placeholder="Votre addresse"
                                    onChange={(e) => {
                                        setAddressLineOne(
                                            e.currentTarget.value
                                        );
                                    }}
                                />
                                <label htmlFor="zip">Code postal</label>
                                <input
                                    id="zip"
                                    type="text"
                                    placeholder="Code postal"
                                    onChange={(e) => {
                                        setPostalCode(e.currentTarget.value);
                                    }}
                                />
                                <label htmlFor="city">Ville</label>
                                <input
                                    id="city"
                                    type="text"
                                    placeholder="Ville"
                                    onChange={(e) => {
                                        setCity(e.currentTarget.value);
                                    }}
                                />
                                <label htmlFor="addressTwo">
                                    Compléments d'adresse
                                </label>
                                <input
                                    id="addressLineTwo"
                                    type="text"
                                    placeholder="Complément d'addresse"
                                    onChange={(e) => {
                                        setAddressLineTwo(
                                            e.currentTarget.value
                                        );
                                    }}
                                />
                                <label htmlFor="ph">Téléphone</label>
                                <input
                                    id="ph"
                                    type="text"
                                    placeholder="numéro de téléphone"
                                    onChange={(e) => {
                                        e.preventDefault();
                                        setPhone(e.currentTarget.value);
                                    }}
                                />
                            </form>
                        </>
                    )}
                </section>
            ) : (
                <section className="delivery-address-box">
                    <h2>Adresse du restaurant</h2>
                    {restaurant && (
                        <>
                            <p>
                                {restaurant.addressLineOne}, {restaurant.postalCode}{" "}
                                {restaurant.city}
                            </p>
                            <div className="map-box">
                                <MapContainer markerPosition={{lat : restaurant.latitude, lng : restaurant.longitude}} initial={{lat : restaurant.latitude, lng : restaurant.longitude}}/>
                            </div>
                        </>

                    )}
                </section>
            )}

            <section className="comment-box">
                <h2>Commentaire</h2>
                <textarea
                    className="comment-input"
                    placeholder="C'est le moment d'ajouter un commentaire à votre commande, si par exemple vous voulez ajouter ou enlever un ingrédient à un produit, ou préciser l'adresse de livraison."
                    onChange={(e) => {
                        setComment(e.currentTarget.value);
                    }}
                ></textarea>
            </section>

            <section className="button-box">
                <h2>Total : {basket.totalPrice} €</h2>
                <button
                    className="button-white"
                    onClick={(e) => {
                        e.preventDefault();
                        handleSubmit();
                    }}
                >
                    Valider et Payer
                </button>
            </section>
        </main>
    );
}
