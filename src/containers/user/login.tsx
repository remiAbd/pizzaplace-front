import {useState, useEffect} from "react"
import { loginUser } from "../../slices/user-slices"
import { useDispatch } from "react-redux"
import { Navigate } from "react-router-dom"

import { login } from "../../api/user-api"
import { Link } from "react-router-dom"


export default function Login() {
    const [email, setEmail] = useState <string> ("")
    const [password, setPassword] = useState <string> ("")
    const [message, setMessage] = useState <string> ("")
    const [redirect, setRedirect] = useState <Boolean> (false)
    const dispatch = useDispatch()

    useEffect(() => {
        //On définit le title de la page
        document.title= "Pizzaplace | Connexion"
    }, [])

    //Fonction de gestion du formulaire
    function handleSubmit() {
        //on set le message d'erreur à vide par défaut
        setMessage("")
        //Si l'utilisateur a rentré un email et un mot de passe
        if(email && password) {
            //On crée l'objet à envoyer au backend
            let datas : {
                email : string,
                password : string
            } = {
                email : email,
                password : password
            }
            //On vérifie les mots de passe
            login(datas)
            .then((res : any) => {
                //On met à jour le message d'erreur avec la réponse du backend
                setMessage(res.msg)
                //Si les mots de passes sont les mêmes
                if(res.status === 200) {
                    //On crée l'objet de connexion utilisateur à envoiyer dans redux
                    let infos : {
                        email : string,
                        key_id : string,
                        role : string,
                        id: number,
                    } = {
                        email : res.results.email,
                        key_id : res.results.key_id,
                        role : "",
                        id : res.results.customersID,
                    }
                    //Si l'utilisateur est un client (i.e il n'a pas de role), on lui donne le role "customer"
                    if(!res.results.role) {
                        infos.role = "customer"
                    //Sinon, on lui donne son role
                    } else {
                        infos.role = res.results.role
                    }
                    //On connecte l'utilisateur dans redux
                    dispatch(loginUser(infos))
                    //on place un token dans le localstorage pour maintenir la connexion lors de la fermeture
                    window.localStorage.setItem("userToken", res.token)
                    //On autorise la redirection
                    setRedirect(true)
                }
            })
            .catch((err : any) => {
                console.log(err)
            })
        //Sinon,
        } else {
            //On met à jour le message d'erreur
            setMessage("veuillez remplir tous les champs svp")
        }
    }

    //Si la redirection est autorisée
    if (redirect) {
        //On redirige vers la page d'accueil
        return <Navigate to="/"/>
    }

    
    return (
        <main className="login">
            <div className="title-box">
                <h1>Connexion</h1>
            </div>
            <form>
                {message && <p className="status-message">{message}</p>}
                <label htmlFor="email">Email</label>
                <input 
                    id="email"
                    type="text"
                    name="email" 
                    placeholder="email"
                    onChange={(e) => {
                        e.preventDefault
                        setEmail(e.currentTarget.value)
                    }}
                />
                <label htmlFor="password">Mot de passe</label>
                <input 
                    id="password"
                    type="password"
                    name="password" 
                    placeholder="mot de passe"
                    onChange={(e) => {
                        e.preventDefault
                        setPassword(e.currentTarget.value)
                    }}
                />

                <button
                    type="submit" 
                    className="button-white"
                    onClick={(e) => {
                        e.preventDefault()
                        handleSubmit()
                    }}>
                    Se connecter
                </button>

                <div className="connexion-navigation">
                    <p>Mot de passe oublié ? <Link to="/forgot">Rénitialiser le ici</Link></p>
                    <p>Vous n'êtes pas inscrit ? <Link to="/register">Inscrivez vous ici</Link></p>
                </div>
            </form>

            
        </main>
    )
}