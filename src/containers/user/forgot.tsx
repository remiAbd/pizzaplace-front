import { useState, useEffect } from "react"
import { forgot } from "../../api/user-api"


export default function Forgot() {
    const [email, setEmail] = useState <string> ("")
    const [message, setMessage] = useState <string> ("")

    useEffect(() => {
        //On définit le title de la page
        document.title= "Pizzaplace | Oubli"
    }, [])

    //Fonction de gestion de formulaire
    function handleSubmit () {
        //On set le message d'erreur par défaut à vide
        setMessage("")
        //On construit l'objet à envoyer au backend
        let datas : {
            email : string
        } = {
            email : email
        }
        //On appelle la fonction d'oubli de mot de passe
        forgot(datas)
        .then((res : any) => {
            //On met à jours le message d'erreur avec la réponse du backend
            setMessage(res.msg)
        })
        .catch((err :any) => {
            console.log(err)
        })
    }

    return (
        <main className="login">
            <div className="title-box">
                <h1>Rénitialisation</h1>
            </div>
            <form>
            {message && <p className="status-message">{message}</p>}
                <label htmlFor="email">Email</label>
                <input
                    id="email"
                    type="text"
                    name="email"
                    placeholder="Entrez votre e-mail"
                    onChange={(e) => {
                        e.preventDefault()
                        setEmail(e.currentTarget.value)
                    }}
                />
                <button
                    type="submit"
                    className="forgot"
                    onClick={(e) => {
                        e.preventDefault()
                        handleSubmit()
                    }}
                >Valider</button>
            </form>
        </main>
    )
}