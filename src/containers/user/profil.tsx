import { useEffect, useState } from "react";
import { editOneCustomer, getOneCustomer, deleteOneCustomer, unvalidate } from "../../api/user-api";
import { useSelector } from "react-redux";
import { selectUser } from "../../slices/user-slices";
import { Navigate } from "react-router-dom";
import moment from "moment";

import ConfirmPopup from "../../components/popup/confirm-popup";
import { getallOrdersFrom } from "../../api/orders-api";
import OrderDetails from "../admin/orders/order-details";


export default function Profil() {
    const user = useSelector(selectUser)
    console.log(user)
    const [message, setMessage] = useState <string> ("")

    const [firstName, setFirstName] = useState <string> ("")
    const [lastName, setLastName] = useState <string> ("")
    const [email ,setEmail] = useState <string> ("")
    const [phone , setPhone] = useState <string> ("")
    const [addressLineOne, setAddressLineOne] = useState <string> ("")
    const [addressLineTwo, setAddressLineTwo] = useState <string> ("")
    const [postalCode, setPostalCode] = useState <string> ("")
    const [city, setCity] = useState <string> ("")
    const [isVisible, setIsVisible] = useState <Boolean> (false)
    const [showPopup, setShowPopup] = useState <boolean> (false)
    const [redirect, setRedirect] = useState <boolean> (false)
    const [showOrder, setShowOrder] = useState <boolean> (false)
    // const [oldemail, setOldEmail] = useState <string> ("")
    const [orders, setOrders] = useState <any> (null)
    const [orderId, setOrderId] = useState <number> (0)

    //Au chagement du dom et après changement de la variable user
    useEffect(() => {
        //On définit le title de la page
        document.title= "Pizzaplace | Profil"
        //On appelle la fonction getInfos()
        getInfos()
        console.log("fin de useEffect", firstName)
        //On réupère les commandes de l'utilisateurs
        console.log("id : ", user.infos.id)
        getallOrdersFrom(user.infos.id)
        .then((res) => {
            console.log("res : ", res)
            if(res.status === 200) {
                setOrders(res.results)
            } else {
                setMessage(res.msg)
            }
        })
        .catch(err => console.log(err))
    }, [user])

    //fonction qui permet de changer le message d'erreur
    function updateMessage(message : string) {
        setMessage(message)
    }

    //Fonction de fermeture de popup
    function closePopup() {
        setShowPopup(false)
        setShowOrder(false)
    }

    //fonction permettant de récupérer les informations d'un client
    function getInfos() {
        //On récupère les informations d'un client
        getOneCustomer(user.infos.key_id)
        .then((res : any) => {
            console.log(res)
            //Si on récupère bien les informations
            if (res.status === 200) {
                //on met à jours les states concernées
                setFirstName(res.results.firstName)
                setLastName(res.results.lastName)
                setEmail(res.results.email)
                setPhone(res.results.phone)
                setAddressLineOne(res.results.addressLineOne)
                setAddressLineTwo(res.results.addressLineTwo)
                setPostalCode(res.results.postalCode)
                setCity(res.results.city)
            }
        })
        .catch((err :any) => {
            console.log(err)
        })
    }

    //Fonction de gestion du formulaire
    function handleSubmit() {
        //On set le message d'erreur à vide par défaut
        setMessage("")
        //On construit l'objet à envoyer
        let datas : {
            firstName : string,
            lastName : string,
            email : string,
            phone : string,
            addressLineOne : string,
            addressLineTwo : string,
            postalCode : string,
            city : string
        } = {
            firstName : firstName,
            lastName : lastName,
            email : email, 
            phone : phone,
            addressLineOne : addressLineOne,
            addressLineTwo : addressLineTwo,
            postalCode : postalCode,
            city : city
        }
        //On demande la modification de l'utilisateur
        editOneCustomer(datas, user.infos.key_id)
        .then((res: any) => {
            //on modifie le message d'erreur avec la réponse du backend
            setMessage(res.msg)
            //Si l'email n'est plus le même
            // if (oldemail !== email) {
                //On invalide le compte de l'utilisateur
                // unvalidate(user.infos.key_id)
                // .then((res) => {
                    //si on a bien invalidé le compte
                    if(res.status === 200) {
                        //On modifie le message d'erreur
                        setMessage("Utilisateur modifié")
                        //On appelle la fonction getInfos()
                        getInfos()
                    }
                // })
                // .catch((err) => console.log(err))
            // }
        })
        .catch((err) => console.log(err))
        //on ferme le popup
        setIsVisible(false)
    }

    //Fonction de suppression de compte
    function deleteAccount() {
        //On supprime l'utilisateur
        deleteOneCustomer(user.infos.key_id)
        .then((res) => {
            //On met à jour le message d'erreur avec la réponse du backend
            setMessage(res.msg)
            //si on a bien supprimé l'utilisateur
            if(res.status === 200) {
                //On autorise la redirection
                setRedirect(true)
            }
        })
        .catch(err => console.log(err))
    }

    //si la redirection est autorisée
    if(redirect) {
        //On redirige vers la page de déconnexion
        return <Navigate to="/logout"/>
    }

    console.log(orders)
    
    return(
        <main className="profil">
            <div className="title-box">
                <h1>Profil</h1>
            </div>
            
            {!isVisible ?
            <>
                {firstName &&
                    <section className="profil-box">
                        {message && <p className="status-message">{message}</p>}
                        <h2>{firstName} {lastName}</h2>
                        <div className="profil-infos">
                            <h3>Informations</h3>
                            <p>Email : {email}</p>
                            <p>Telephone : {phone}</p>
                            <p>Adresse : {addressLineOne}, {postalCode} {city}</p>
                            {addressLineTwo && <p>Complément d'adresse : {addressLineTwo}</p>}
                        </div>
                        <div className="order-history">
                            <h3>Historique des commandes</h3>
                            {orders && <>
                            {orders.length > 0 ? 
                                orders.map((order : any, index : number) => {
                                    if(index%2 === 0) {
                                        return (
                                            <button
                                                className="invisible-button"
                                                onClick={(e) => {
                                                    e.preventDefault()
                                                    setOrderId(order.ordersID)
                                                    setShowOrder(true)
                                                }}
                                            >
                                                <div 
                                                    className="order-list white"
                                                    key={index}
                                                >
                                                    <p>{moment(order.orderDate).format("DD/MM/YYYY")},</p>
                                                    <p> {order.status},</p>
                                                    <p> {order.amount}€</p>
                                                </div>
                                            </button>
                                        )
                                    } else {
                                        return (
                                            <button
                                                className="invisible-button"
                                                onClick={(e) => {
                                                    e.preventDefault()
                                                    setOrderId(order.ordersID)
                                                    setShowOrder(true)
                                                }}
                                            >
                                                <div 
                                                    className="order-list grey"
                                                    key={index}
                                                >
                                                    <p>{moment(order.orderDate).format("DD/MM/YYYY")},</p>
                                                    <p> {order.status},</p>
                                                    <p> {order.amount}€</p>
                                                </div>
                                            </button>
                                        )
                                    }
                                })
                            :
                                <p className="status-message">Aucunes commandes trouvées</p>
                            }
                            </>
                        }
                        </div>
                        <div className="horizontal">
                            <button 
                                className="button-white"
                                onClick={(e) => {
                                    e.preventDefault()
                                    setIsVisible(true)
                                }}
                            >Modifier vos informations</button>
                            <button 
                                className="button-white"
                                onClick={(e) => {
                                    e.preventDefault()
                                    setShowPopup(true)
                            }}>Supprimer votre compte</button>
                        </div>
                    </section>
                }
            </>
            :
            <>
                <form>
                    <h2>Modification</h2>
                    {message && <p className="status-message">{message}</p>}
                    <label htmlFor="firstName">Prénom</label>
                    <input id="firstName" type="text" placeholder="prénom" defaultValue={firstName} onChange={(e)=> setFirstName(e.currentTarget.value)}/>
                    <label htmlFor="lastName">Nom</label>
                    <input id="lastName" type="text" placeholder="Nom" defaultValue={lastName} onChange={(e)=> setLastName(e.currentTarget.value)}/> 
                    <label htmlFor="email">Email</label>
                    <input id="email" type="text" placeholder="Email" defaultValue={email} onChange={(e)=> setEmail(e.currentTarget.value)}/>
                    <label htmlFor="phone">Télephone</label>
                    <input id="phone" type="text" placeholder="telephone" defaultValue={phone} onChange={(e)=> setPhone(e.currentTarget.value)}/>
                    <label htmlFor="address">Adresse</label>
                    <input id="address" type="text" placeholder="Adresse" defaultValue={addressLineOne} onChange={(e)=> setAddressLineOne(e.currentTarget.value)}/>
                    <label htmlFor="addresstwo">Complément d'adresse</label>
                    <input id="addresstwo" type="text" placeholder="Complément d'adresse" defaultValue={addressLineTwo} onChange={(e)=> setAddressLineTwo(e.currentTarget.value)}/>
                    <label htmlFor="zip">Code postal</label>
                    <input id="zip" type="text" placeholder="Code postal" defaultValue={postalCode} onChange={(e)=> setPostalCode(e.currentTarget.value)}/>
                    <label htmlFor="city">Ville</label>
                    <input id="city" type="text" placeholder="ville" defaultValue={city} onChange={(e)=> setCity(e.currentTarget.value)}/>
                    
                    <div className="button-box">
                        <button 
                            className="button-white"
                            onClick={(e) => {
                                e.preventDefault()
                                handleSubmit()
                            }}
                        >Sauvegarder</button>

                        <button 
                            className="button-white"
                            onClick={(e) => {
                                e.preventDefault()
                                setIsVisible(false)
                            }}
                        >Annuler</button>
                    </div>
                </form>
            </>
            }
            {showPopup &&
                <ConfirmPopup
                    onClose={closePopup}
                    updateMessage={updateMessage}
                    delete={deleteAccount}
                />
            }
            {showOrder &&
                <OrderDetails 
                    updateMessage={updateMessage}
                    onClose={closePopup}
                    id = {orderId}
                />
            }

        </main>
    )
}