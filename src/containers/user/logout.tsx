import { useEffect, useState } from "react"
import { logoutUser } from "../../slices/user-slices"
import { useDispatch } from "react-redux"
import { Navigate } from "react-router-dom"



export default function Logout() {
    const dispatch = useDispatch()
    const [redirect, setRedirect] = useState <boolean> (false)

    //Au chargement du dom
    useEffect(() => {
        //On supprime le token dans le localstorage
        window.localStorage.removeItem("userToken")
        //On déconnecte l'utilisateur de redux
        dispatch(logoutUser())
        //On autorise la redirection
        setRedirect(true)
    }, [])

    //si la redirection est autorisée
    if (redirect) {
        //On redirige vers la home
        return <Navigate to="/"/>
    }

    return (
        <main>
        </main>
    )
}