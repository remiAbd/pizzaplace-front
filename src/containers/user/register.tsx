import { useState, useEffect } from "react";
import { registerOneCustomer } from "../../api/user-api"
import { Link, Navigate } from "react-router-dom";


export default function Register() {
    //On initialise les states qui serviront à accueuillir les informations de l'utilisateur
    const [firstName, setFirstName] = useState <string> ("")
    const [lastName, setLastName] = useState <string> ("")
    const [email, setEmail] = useState <string> ("")
    const [phone, setPhone] = useState <string> ("")
    const [password1, setPassword1] = useState <string> ("")
    const [password2, setPassword2] = useState <string> ("")
    const [addressLineOne, setAddressLineOne] = useState <string> ("")
    const [addressLineTwo, setAddressLineTwo] = useState <string> ("")
    const [postalCode, setPostalCode] = useState <string> ("")
    const [city, setCity] = useState <string> ("")
    const [message, setMessage] = useState <string> ("")
    //On initialise la statez de redirection
    const [redirect, setRedirect] = useState <boolean> (false)

    useEffect(() => {
        //On définit le title de la page
        document.title= "Pizzaplace | Inscription"
    }, [])
    //Fonction de gestion du formulaire
    function handleSubmit() {
        //On set le message d'erreur à vide par défaut
        setMessage("")
        //Si tous les champs obligatoires sont remplis
        if(firstName && lastName && email && phone && password1 && password2 && addressLineOne && postalCode && city) {
            //Si les mots de passe correspondent
            if (password1 === password2) {
                //On construit l'object qu'on va envoyer au backend
                let datas : {
                    firstName : string,
                    lastName : string,
                    email : string,
                    phone : string,
                    password : string,
                    addressLineOne : string,
                    addressLineTwo : string,
                    postalCode : string,
                    city : string
                } = {
                    firstName : firstName,
                    lastName : lastName,
                    email : email, 
                    phone : phone,
                    password : password1,
                    addressLineOne : addressLineOne,
                    addressLineTwo : addressLineTwo,
                    postalCode : postalCode,
                    city : city
                }
                //On appelle le backend pour enregistrer l'utilisateur
                registerOneCustomer(datas)
                .then((res) => {
                    //On met à jours le message d'erreur
                    setMessage(res.msg)
                    //Si l'enregistrement a réussi
                    if(res.status === 200) {
                        //On autorise la redirection
                        setRedirect(true)
                    }
                })
                .catch((err) => {
                    console.log(err)
                })
            //Sinon,
            } else {
                //On informe l'utilisateur
                setMessage("Les mots de passe ne correspondent pas")
            }
        //Sinon,
        } else {
            //On informe l'uilisateur
            setMessage("Veuillez remplir tous les champs munis d'un * s'il vous plait'")
        }
    }

    //si la redirection est autorisée
    if(redirect) {
        //On renvoi l'utilisateur sur la page de connexion
        return <Navigate to="/login"/>
    }

    return(
        <main className="login">
            <div className="title-box">
                <h1>Inscription</h1>
            </div>
            <form>
                {message && <p className="status-message">{message}</p>}
                <label htmlFor="firstName">Prénom*</label>
                <input 
                    id="firstName" 
                    type="text"
                    name="firstName"
                    placeholder="Prénom*"
                    onChange={(e) => {
                        e.preventDefault
                        setFirstName(e.currentTarget.value)
                    }}
                />
                <label htmlFor="lastName">Nom*</label>
                <input 
                    id="lastName" 
                    type="text"
                    placeholder="Nom*"
                    name="lastName" 
                    onChange={(e) => {
                        e.preventDefault
                        setLastName(e.currentTarget.value)
                    }}
                />
                <label htmlFor="email">Email*</label>
                <input 
                    id="email" 
                    type="text"
                    placeholder="Adresse e-mail*"
                    name="email" 
                    onChange={(e) => {
                        e.preventDefault
                        setEmail(e.currentTarget.value)
                    }}
                />
                <label htmlFor="phone">Télephone*</label>
                <input 
                    id="phone" 
                    type="text"
                    placeholder="Numéro de télephone*"
                    name="phone" 
                    onChange={(e) => {
                        e.preventDefault
                        setPhone(e.currentTarget.value)
                    }}
                />
                <label htmlFor="password">Mot de passe*</label>
                <input 
                    id="password" 
                    type="password"
                    placeholder="Mot de passe*"
                    name="password1" 
                    onChange={(e) => {
                        e.preventDefault
                        setPassword1(e.currentTarget.value)
                    }}
                />
                <label htmlFor="password2">Confirmez votre mot de passe*</label>
                <input 
                    id="passwordtwo" 
                    type="password"
                    placeholder="Confirmez votre mot de passe*"
                    name="password2" 
                    onChange={(e) => {
                        e.preventDefault
                        setPassword2(e.currentTarget.value)
                    }}
                />
                <label htmlFor="address">Adresse*</label>
                <input 
                    id="address" 
                    type="text"
                    placeholder="Adresse*"
                    name="address1" 
                    onChange={(e) => {
                        e.preventDefault
                        setAddressLineOne(e.currentTarget.value)
                    }}
                />
                <label htmlFor="addresstwo">Complément d'adresse</label>
                <input 
                    id="addresstwo" 
                    type="text"
                    placeholder="Compléments d'adresse"
                    name="address2" 
                    onChange={(e) => {
                        e.preventDefault
                        setAddressLineTwo(e.currentTarget.value)
                    }}
                />
                <label htmlFor="zip">Code postal*</label>
                <input 
                    id="zip" 
                    type="text"
                    placeholder="Code postal*"
                    name="postalCode" 
                    onChange={(e) => {
                        e.preventDefault
                        setPostalCode(e.currentTarget.value)
                    }}
                />
                <label htmlFor="city">Ville*</label>
                <input 
                    id="city" 
                    type="text"
                    placeholder="Ville*"
                    name="city" 
                    onChange={(e) => {
                        e.preventDefault
                        setCity(e.currentTarget.value)
                    }}
                />
                <button
                    className="submit-button button-white" 
                    type="submit" 
                    onClick={(e) => {
                        e.preventDefault()
                        handleSubmit()
                    }}
                >S'inscrire</button>

                <div className="connexion-navigation">
                    <p>Déja inscrit ? <Link to="/login">Connectez vous ici</Link></p>
                </div>
            </form>

            
        </main>
    )
}