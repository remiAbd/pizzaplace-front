//Popup de confirmation de suppression de compte
export default function ConfirmPopup(props : any) {

    return(
        <section className="big-popup">
            <h1>Attention</h1>
            <p>Cette action supprimera définitivement votre compte, il ne sera pas possible de le retrouver par la suite</p>
            <p>Voulez vous vraiment supprimer votre compte</p>
            <div className="horizontal">
                <button
                    className="button-white"
                    onClick={(e) => {
                        e.preventDefault()
                        props.onClose()
                    }}
                >Non</button>
                <button
                    className="button-white"
                    onClick={(e)=> {
                        e.preventDefault()
                        props.onClose()
                        props.delete()
                    }}
                >Oui</button>
            </div>
        </section>
    )
}