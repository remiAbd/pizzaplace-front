import { useEffect, useRef } from "react"

export default function Popup(props : any) {
    const popupRef = useRef(null)

    useEffect(() => {
        const handleClickOutside = (event) => {
            if (popupRef.current && !popupRef.current.contains(event.target)) {
              props.onClose();
            }
          };
      
          document.addEventListener('mousedown', handleClickOutside);
      
          return () => {
            document.removeEventListener('mousedown', handleClickOutside);
          };
    }, [])

    if(!props.isOpen) {
        return null
    } else {
        return(
            <div 
                className={`timed-small-popup ${props.isOpen ? 'active' : ''}`}
                ref={popupRef}
            >
                <p>{props.content}</p>
            </div>
        )
    }
}