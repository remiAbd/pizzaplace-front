import React, { Component } from 'react';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';

interface MapContainerProps {
  google: any;
  markerPosition: { lat: number; lng: number };
  initial: { lat: number; lng: number };
}

class MapContainer extends Component<MapContainerProps> {
  render() {
    const { google, markerPosition, initial } = this.props;

    if (!google) {
      return null; // Don't render the map if Google Maps API isn't loaded
    }

    return (
      <Map
        google={google}
        zoom={13} // Zoom level
        initialCenter={initial}
        center={initial} // Center the map on the marker
      >
        <Marker name={'Restaurant Location'} title={"Restaurant location"} position={markerPosition} />
      </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyCZboW9hSTCmc5lpYlDNRMoBUcm5CXn6nI', // Replace with your actual API key
})(MapContainer);
