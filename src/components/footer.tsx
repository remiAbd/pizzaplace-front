
export default function Footer() {
    return (
        <footer>
                <p>PizzaPlace, tout droits réservés</p>
                <p>Contact : Par mail à pizzaplace@gmail.com ou par télephone au 06 66 66 66 66</p>
        </footer>
    )
}