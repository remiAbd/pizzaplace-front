import { Link } from "react-router-dom"
import { useSelector } from "react-redux"
import { useState, useEffect, useRef } from "react"

import { selectUser } from "../slices/user-slices"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faUser, faCartShopping } from "@fortawesome/free-solid-svg-icons"

export default function Header() {
    const burgerRef = useRef <any> (null)
    let user = useSelector(selectUser)
    const [burgerVisible, setBurgerVisible] = useState <Boolean> (false)
    // const [user, setUser] = useState <any> (useSelector(selectUser))

    //Au chargement du dom et à l'actualisation de la variable user
    useEffect(() => {
        //Fonction de fermeture du burger menu
        function handleClickOutside(e : any) {
            if(burgerRef.current && !burgerRef.current.contains(e.target)) {
                setBurgerVisible(false)
            }
        }
        //Ecouteur d'evenement pour fermer le burger menu
        document.addEventListener("mousedown", handleClickOutside)
        return () => {
            document.removeEventListener('mousedown', handleClickOutside);
          };
    }, [user])


    //Fonction de gestion du burger menu
    function BurgerMenu() {
        //Si l'utilisateur est déconnecté
        if (!user.isLogged) {
            //alors on lui présente ce menu
            return (
                <>
                    <Link to="/register">Inscription</Link>
                    <Link to="/login">Se connecter</Link>
                </>
            )
        //Si l'utilisateur est connecté et client
        } else if (user.infos.role === "customer") {
            //alors on lui présente ce menu
            return (
                <>
                    <Link to="/profil">Profil</Link>
                    <Link to="/logout">Déconnexion</Link>
                </>
            )
        //Si l'utilisateur est connecté et admin
        } else if (user.infos.role === "admin") {
            //alors on lui présente ce menu
            return (
                <>
                    <Link to="/admin">Administration</Link>
                    <Link to="/order/current">Commandes en cours</Link>
                    <Link to="/order/past">Commandes passées</Link>
                    <Link to="/logout">Déconnexion</Link>
                </>
            )
        } else if (user.infos.role === "employee") {
            return (
                <>
                    <Link to="/order/current">Commandes en cours</Link>
                    <Link to="/order/past">Commandes passées</Link>
                    <Link to="/logout">Déconnexion</Link>
                </>
            )
        }
        return (<></>)
    }

    
    return (
    <header>
        <nav className="movable">
            <Link to="/home">Home</Link>
            <Link to="/products">Carte</Link>
            <Link to="/basket"><FontAwesomeIcon icon={faCartShopping}/></Link>
        </nav>
        <button 
            className="invisible-button"
            onClick={() => {
                setBurgerVisible(!burgerVisible)
            }}
        ><FontAwesomeIcon icon={faUser}/></button>
        {burgerVisible && 
            <nav className="Burger-user" ref={burgerRef}>
                <BurgerMenu/>
            </nav>
        }
    </header>
    )
}