import {useState} from 'react'
// import {loadStripe} from '@stripe/stripe-js'
import {CardElement, useStripe, useElements} from '@stripe/react-stripe-js'
// import {Navigate} from 'react-router-dom'
import {useSelector, useDispatch} from 'react-redux' 
import {selectUser} from '../slices/user-slices'
import {deleteBasket, selectBasket} from '../slices/basket-slices'

import {checkPayment, setPaid } from '../api/orders-api'
import { Navigate } from 'react-router-dom'

export default function CheckoutForm(props: any) {
    const [message, setMessage] = useState <any> ("")
    const [email, setEmail] = useState <string> ("")
    const [redirectSuccess, setRedirectSuccess] = useState <boolean> (false)
    const basket = useSelector(selectBasket)
    const user = useSelector(selectUser)
    const dispatch = useDispatch()
    const [secret, setSecret] = useState <any> (null)

    const stripe = useStripe() //on va pouvoir utiliser les fonctions de l'api stripe
    const elements = useElements() //on va pouvoir utiliser des éléments de consommation de la carte
        


    async function handleSubmit(e: any) {
        e.preventDefault()
        if(!stripe || !elements){
            setMessage("Oups un problème est survenue avec le terminal de paiement!")
            console.log("message : ", message)
            return null
        }
        if (user.isLogged) {
            setEmail(user.email)
            var data : {
                email : string,
                orderId : number,
            } = {
                email : user.email,
                orderId : props.orderId
            }
        } else {
            var data : {
                email : string,
                orderId : number
            } = {
                email : "",
                orderId : props.orderId
            }
        }

        const paymentIntent = await checkPayment(data)
        if (paymentIntent.status === 500) {
            setMessage("payment invalide")
            console.log(paymentIntent)
        } else {
            console.log(paymentIntent)
            setSecret(paymentIntent.client_secret)
        }

        const cardElement = elements.getElement(CardElement)
        const payment = await stripe.confirmCardPayment(paymentIntent.client_secret ,{
            payment_method : {
                card : cardElement,
                billing_details : {
                    email : email
                }
            }
        })
        console.log('payment : ', payment)
        if(payment.error){
            console.log("error", payment)
            setMessage(payment.error.message)
        }else{
            //si le paiement est réussi
            if(payment.paymentIntent.status === "succeeded"){
                console.log("paiment effectué")
                //on enregistre dans la bdd que la status de sa commande est payée
                setPaid(props.orderId)
                .then((res)=>{
                    console.log(res)
                    if(res.status === 200){
                        //On supprime le panier de l'utilisateur
                        dispatch(deleteBasket())
                        window.localStorage.removeItem("basket")
                        //On redirige l'utilisateur
                        setRedirectSuccess(true)
                    } else {
                        setMessage("Une erreur est survenue, veuillez réessayer")
                    }
                })
                .catch(err=>console.log(err))
            }
        }
    }

    console.log("stripe and elements", stripe, elements, message)

    if(redirectSuccess) {
        return <Navigate to="/payment/success" state={{ orderId: props.orderId }} />
    }

    return (
        <section>
            {message && <p>{message}</p>}
            <form
                onSubmit={(e) => {
                    handleSubmit(e)
                }}
            >
                {stripe && elements && 
                    <CardElement
                        id="card-element-id"
                        options={{
                        style: {
                            base: {
                            fontSize: '16px',
                            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                            },
                            invalid: {
                            color: '#dc3545',
                            },
                        },
                        }}
                    />
                }
                <input type="submit"/>
            </form>
        </section>
    )
}