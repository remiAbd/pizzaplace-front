import axios from "axios"
import { config } from "../config"
//On récupère le token de l'utilisateur dans le localstorage
const token : string | null = window.localStorage.getItem("userToken")


//Requête api de connexion d'un utilisateur 
export function login(datas : any) {
    return axios.post(`${config.api_url}api/v01/user/login`, datas)
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

  //////////////////////////////////////////
 //////////////////////////////////////////
//////////////////////////////////////////


//Requête api de sauvegarde d'un client
export function registerOneCustomer(datas : any) {
    return axios.post(`${config.api_url}api/v01/customer/save`, datas, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête api de modification d'un client
export function editOneCustomer(datas : any, key_id : string) {
    return axios.put(`${config.api_url}api/v01/customer/edit/${key_id}`, datas, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête api de suppression d'un client
export function deleteOneCustomer(key_id : string) {
    return axios.delete(`${config.api_url}api/v01/customer/delete/${key_id}`, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête api de récupération de tous les clients
export function getAllCustomers() {
    let token2 : string | null = window.localStorage.getItem("userToken")
    return axios.get(`${config.api_url}api/v01/customer/all`, {headers : {"x-access-token" : token2}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête API pour récupérer un client
export function getOneCustomer(key_id: string) {
    let token2 : string | null = window.localStorage.getItem("userToken")
    return axios.get(`${config.api_url}api/v01/customer/${key_id}`, {headers : {"x-access-token" : token2}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête API pour valider la key_id d'un client
export function validateCustomer(key_id : string) {
    return axios.post(`${config.api_url}api/v01/customer/validation/${key_id}`)
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête API pour gérer l'oubli de mot de passe
export function forgot(datas : any) {
    return axios.post(`${config.api_url}api/v01/customer/forgot/`, datas)
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête API pour enlever la validation d'un compte
export function unvalidate(key_id : string) {
    return axios.put(`${config.api_url}api/v01/customer/unvalidate/${key_id}`, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

  ////////////////////////////////////////
 ////////////////////////////////////////
////////////////////////////////////////


//Requête api d'ajout d'un employé
export function saveOneEmployee(datas : any) {
    return axios.post(`${config.api_url}api/v01/employee/save`, datas, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête api de modification d'un employé
export function editOneEmployee(datas : any, id : number) {
    return axios.put(`${config.api_url}api/v01/employee/edit/${id}`, datas, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête api de suppression d'un employé
export function deleteOneEmployee(id : number) {
    return axios.delete(`${config.api_url}api/v01/employee/delete/${id}`, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête api de récupération de tous les employés
export function getAllEmployees() {
    return axios.get(`${config.api_url}api/v01/employee/all`, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête API de récupération d'un employé par son id
export function getOneEmployee(id : number) {
    return axios.get(`${config.api_url}api/V01/employee/one/${id}`, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}
