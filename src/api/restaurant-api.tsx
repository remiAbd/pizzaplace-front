//On récupère le token de l'utilisateur dans le localstorage
const token : string | null = window.localStorage.getItem("userToken")
import axios from "axios"
import { config } from "../config"


//Requête API pour récupérer les infos d'un restaurant
export function getRestaurant() {
    return axios.get(`${config.api_url}api/V01/restaurant/get`)
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour sauvegarder un restaurant
export function saveRestaurant(datas : any) {
    return axios.post(`${config.api_url}api/V01/restaurant/save`, datas, {headers : {"x-access-token" : token}})
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour modifier les informations d'un restaurant
export function updateRestaurant(id : number, datas : any) {
    return axios.put(`${config.api_url}api/V01/restaurant/edit${id}`, datas, {headers : {"x-access-token" : token}})
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour supprimer un restaurant
export function deleteRestaurant(id : number) {
    return axios.delete(`${config.api_url}api/V01/restaurant/delete${id}`, {headers : {"x-access-token" : token}})
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}