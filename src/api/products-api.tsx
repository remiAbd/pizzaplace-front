import axios from "axios"
import { config } from "../config"
//On récupère le token de l'utilisateur
const token : string | null = window.localStorage.getItem("userToken")


//Requête api de sauvegarde d'un produit
export function saveOneProduct(datas : any) {
    return axios.post(`${config.api_url}api/v01/products/save`, datas, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête API pour sauvegarder une image
export function saveOneImage(datas : any) {
    console.log("token : ", token)
    return axios.post(`${config.api_url}api/v01/image/save`, datas, {headers : {"x-access-token" : token, "Content-Type" : "multipart/form-data"}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête api de modification d'un produit
export function editOneProduct(datas : any, id : number) {
    console.log("id : ", id)
    return axios.put(`${config.api_url}api/v01/products/edit/${id}`, datas, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête api de suppression d'un produit
export function deleteOneProduct(id : number) {
    return axios.delete(`${config.api_url}api/v01/products/delete/${id}`, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête api de récupération de tous les produits
export function getAllProducts() {
    return axios.get(`${config.api_url}api/v01/products/all`)
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête API pour récuperer un produit
export function getOneProduct(id : number) {
    return axios.get(`${config.api_url}api/v01/products/one/${id}`)
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}


  /////////////////////////////////////////
 /////////////////////////////////////////
/////////////////////////////////////////

//Requête api de sauvegarde d'une catégorie
export function saveOneCategory(datas : any) {
    return axios.post(`${config.api_url}api/v01/categories/save`, datas, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête api de modification d'un produit
export function editOneCategory(datas : any, id : number) {
    return axios.put(`${config.api_url}api/v01/categories/edit/${id}`, datas, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête api de suppression d'un produit
export function deleteOneCategory(id : number) {
    return axios.delete(`${config.api_url}api/v01/categories/delete/${id}`, {headers : {"x-access-token" : token}})
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête api de récupération de tous les produits
export function getAllCategories() {
    return axios.get(`${config.api_url}api/v01/categories/all`)
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}

//Requête API pour récupérer une catégorie
export function getOneCategory(id : number) {
    return axios.get(`${config.api_url}api/v01/categories/${id}`)
    .then((res : any) => {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}