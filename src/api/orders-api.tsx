import  axios  from "axios";
import { config } from "../config";
//On récupère le token de l'utilisateur
const token = window.localStorage.getItem("userToken")

//Requête API pour récupérer toutes les commandes
export function getAllOrders() {
    let token2 : string | null = window.localStorage.getItem("userToken")
    return axios.get(`${config.api_url}api/V01/orders/all`, {headers : {"x-access-token" : token2}})
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour récupérer une commande
export function getOneOrder(id : number) {
    return axios.get(`${config.api_url}api/V01/order/one/${id}`)
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour enregistrer une commande
export function saveOneOrder(datas : any) {
    return axios.post(`${config.api_url}api/V01/order/save/`, datas)
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour initialiser un paiment avec stripe
export function checkPayment(datas : any) {
    return axios.post(`${config.api_url}api/V01/order/payment/`, datas)
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour changer le status d'une commande
export function changeStatus(id : number, datas : any) {
    let token2 : string | null = window.localStorage.getItem("userToken")
    return axios.put(`${config.api_url}api/V01/order/changeStatus/${id}`, datas, {headers : {"x-access-token" : token2}})
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour récupérer les détails d'une commande
export function getOrderDetails(id : number) {
    return axios.get(`${config.api_url}api/V01/order/details/${id}`)
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour récupérer tous les détails d'une commande
export function getFullDetails(id : number) {
    return axios.get(`${config.api_url}api/V01/order/full/${id}`)
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour définir une commande comme envoyée
export function setShipped(id : number) {
    let token2 : string | null = window.localStorage.getItem("userToken")
    console.log("on passe ici, token : ", token2, token)
    return axios.put(`${config.api_url}api/V01/order/setShipped/${id}`, {headers : {"x-access-token" : token2}})
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour récupérer toutes les commandes en cours 
export function getAllCurrentOrders() {
    return axios.get(`${config.api_url}api/V01/orders/current`, {headers : {"x-access-token" : token}})
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour récupérer toutes les commandes passées
export function getAllPastOrders() {
    return axios.get(`${config.api_url}api/V01/orders/past`, {headers : {"x-access-token" : token}})
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

//Requête API pour récupérer toutes les commandes d'un client
export function getallOrdersFrom(id : number) {
    let token2 : string | null = window.localStorage.getItem("userToken")
    return axios.get(`${config.api_url}api/V01/orders/from/all/${id}`, {headers : {"x-access-token" : token2}})
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}

export function setPaid(id : number) {
    return axios.put(`${config.api_url}api/V01/orders/setPaid/${id}`)
    .then((res) => {
        return res.data
    })
    .catch((err) => {
        return err
    })
}