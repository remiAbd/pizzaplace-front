import axios from "axios"
import {config} from "../config"
//On récupèree le token de l'utilisateur dans le localstorage
const token : any = window.localStorage.getItem('userToken')

//Requête API pour vérifier le token d'un utilisateur
export function checkToken() {
    let token2 : any = window.localStorage.getItem("userToken")
    return axios.get(`${config.api_url}api/v01/auth/checkToken`, {headers : {"x-access-token" : token2}})
    .then((res : any)=> {
        return res.data
    })
    .catch((err : any) => {
        return err
    })
}